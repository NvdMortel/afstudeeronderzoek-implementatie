<?php get_header(); 

$algemeen_leerdoel_havo = get_field("algemeen_leerdoel_havo");
$algemeen_leerdoel_vwo = get_field("algemeen_leerdoel_vwo");

$classes = array();
$onderwerpen = array();

$examenstof_havo = array();
$domein_havo = array();
$eindterm_havo = array();

$examenstof_vwo = array();
$domein_vwo = array();
$eindterm_vwo = array();

?>

<main class="wrapper">	

	<!-- Sidebar -->
	<nav id="sidebar">
		<div class="sidebar-header">
			
			<button type="button" id="sidebarCollapse" class="btn btn-info">
				<b> < </b>
			</button>
		</div>

		
			<b>OVERZICHT</b>
			<li>
				<a href="#vakpagina" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Vakpagina</a>
			</li>

			<br><b>VAKKEN</b>
			<ul class="list-unstyled components">

			<? 

           // $category = get_the_category();
			//$thiscat = $category->term_id; 

            $thiscat =  get_query_var('cat'); //
            $rootParent = $thiscat;
            $cat_parent_id=0;

            while ($rootParent!=null & $rootParent!=0) {
            	$current_term = get_term($rootParent); 
            	$rootParent = $current_term->parent; 
            	if($rootParent!=null & $rootParent!=0){
            		$cat_parent_id = $rootParent;
            	}else{
            		$cat_parent_id = $current_term->term_id;
            	}
            }

            $rootParent = $cat_parent_id; //vak 


            $catobject = get_category($thiscat,false); // Get the Category object by the id of current category

			$parentcat = $catobject->category_parent; // the id of the parent category 

			$cat_args = array(
				'parent'  => $rootParent,
				'hide_empty' => 0,
				'order'    => 'ASC',
				);

			$categories = get_categories($cat_args);

			foreach($categories as $category){

				if ($category->term_id == $thiscat) { $extraClass = 'active';}
				else { $extraClass = '';}
				//echo '<li>> <a href="#'.$category->slug . '" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle '.$extraClass.'">' . $category->name . '</a></li>';
				echo '<li> <a class="'.$category->slug . ' ' . $extraClass . '" href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
				//echo '<ul class="sublist" class="collapse list-unstyled" id="'.$category->slug . '">';
				$cat_args_sub = array(
					'parent'  => $category->term_id,
					'hide_empty' => 0,
					'order'    => 'ASC',
					);

				$categories_sub = get_categories($cat_args_sub);

				foreach($categories_sub as $category_sub){

					if ($category_sub->term_id == $thiscat) { $extraClass = 'active';}
					else { $extraClass = '';}
					//echo '<li><a id="'.$category->slug . ' ' . $extraClass . '" href="' . get_category_link($category_sub->term_id) . '">' . $category_sub->name . '</a></li>'; 
				}
				//echo '</ul>';           
			}

			?>
			<li>
				<a href="">Alle onderwerpen</a> 
			</li>

			

        </ul>


    </nav>
    <!-- Page Content -->
    <div id="content">
    	<!-- We'll fill this with dummy content -->

    	<div class="container-fluid">

    		<h2>Impactsaldo</h2>
    		<p>Hoogte: €3242</p>

    		<br>
    		<a href="" target="_blank">Fonds vullen ></a>
    		<br>
    		<a href="" target="_blank">Investeren ></a>
    		<br>
    		<a href="" target="_blank">Doneren ></a>
    	
    		<br>
    		<a href="" target="_blank"  data-toggle="modal" data-target="#ModalLoginForm">Projectvoorstel schrijven ></a>
			
				<!-- Modal HTML Markup -->
				<div id="ModalLoginForm" class="modal fade">
				    <div class="modal-dialog" role="document">
				        <div class="modal-content">
				            <div class="modal-header">
				                <h1 class="modal-title">Start</h1>
				            </div>
				            <div class="modal-body">
				                <?php echo do_shortcode( '[wpuf_form id="7877"]' ); ?>
				            </div>
				        </div><!-- /.modal-content -->
				    </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
	<br><br>
    		<h2>Opdrachten</h2>

    	<?php 
			// the query
			$wpb_all_query = new WP_Query(array('post_type'=>'project', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
			 
			<?php if ( $wpb_all_query->have_posts() ) : ?>
			 
			<ul>
			    <!-- the loop -->
			    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
			        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			    <?php endwhile; ?>
			    <!-- end of the loop -->
			</ul>
			 
			<?php wp_reset_postdata(); ?>
			 
			<?php else : ?>
			    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
    		
    	</div>
    </div>

	       
</main>

	   



	    <?php include(locate_template('/includes/include-contactdetails.php'));
$footer_codes = get_field('footer_codes', 'option');
 wp_footer(); ?> 
  <? if($footer_codes): echo $footer_codes; endif ?>   
  </body>
</html><?
//get_footer(); ?>