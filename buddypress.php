<?php acf_form_head(); ?>
<?php get_header(); 

$tijd = array(0);
$mutatie_saldo = array(0);
$mutatie_punten = array(0);
$mutatie_zegels = array(0);

if( have_rows('transacties', 'user_'.bp_displayed_user_id())):

	while( have_rows('transacties', 'user_'.bp_displayed_user_id()) ) : the_row();

		array_push($tijd, get_sub_field('tijdstempel'));

		if (get_sub_field('impactsaldo')) {
			array_push($mutatie_saldo, get_sub_field('impactsaldo'));
		}
		else {
			array_push($mutatie_saldo, 0);
		}
		if (get_sub_field('impactpunten')) {
			array_push($mutatie_punten, get_sub_field('impactpunten'));
		}
		else {
			array_push($mutatie_punten, 0);
		}
		if (get_sub_field('impactzegels')) {
			array_push($mutatie_zegels, get_sub_field('impactzegels'));
		}
		else {
			array_push($mutatie_zegels, 0);
		}

	endwhile;?>
<?php endif; ?>

<? $lijst = array();
$users = get_users( array( 'fields' => array( 'ID' )));

foreach($users as $user_id){
	$aantal = get_field('impactlijst_totaal', 'user_'.$user_id->ID);
	$lijst[$user_id->ID] = $aantal;
}

arsort($lijst);

?>

<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/leden.css">

<? if (get_current_user_id() == bp_displayed_user_id() || get_current_user_id() == 1) { ?>
	<script src="<?=get_template_directory_uri()?>/js/sidebar-menu.js"></script>
<? } ?>


<main class="wrapper">
	
	<? if (get_current_user_id() == bp_displayed_user_id() || get_current_user_id() == 1) { ?>
		<!-- Sidebar -->
		<nav class="sidebar">

			<b style="padding-left:10px;">IMPACTPROGRAMMA</b>
			<br>

			<a id="dashboard-link">Dashboard</a>

			<a id="informatie-link">Totaaloverzicht</a>

			<a id="impactlijst-link">Impactlijst</a>

			<a id="administratie-link">Administratie</a>

			<a id="profiel-link">Openbaar profiel</a>


		<!-- <br>
		<b>MIJN VAKKENNIS</b>
		<li>
			<a href="/<?echo get_cat_name($rootParent); ?>" >Natuurkunde</a>
		</li>

		<br>
		<b>MIJN TOOLS</b>
		<li>
			<a href="/<?echo get_cat_name($rootParent); ?>" >Opgavenbank</a>
		</li>
		<li>
			<a href="/<?echo get_cat_name($rootParent); ?>" >SE-generator</a>
		</li> -->

	<!-- 	<br>
		<b>MIJN INSTELLINGEN</b> -->

		<a id="accountgegevens-link">Accountgegevens</a>
		
	</nav>
<? } ?>


<!-- Page Content -->
<div id="content">

	<div class="container">

		<div class="row">

				<?// if (get_current_user_id() == bp_displayed_user_id() || get_current_user_id() == 1 || get_current_user_id() == 1) {
					if (get_current_user_id() == bp_displayed_user_id() || get_current_user_id() == 1) {

						?><div class="col-md-9">

							<h1>Welkom <?php bp_displayed_user_username(); ?>,
								<img id="empower" src="<?=get_template_directory_uri()?>/images/empower.png">
							</h1>
							</div><?
						}

						else {
							?><div class="col-md-9">
								<b>OPENBAAR PROFIEL</b>
								<h1><? bp_displayed_user_username(); ?></h1>
								<!-- //TODO private maken -->
								<script>$('#profiel').show(); </script>
							</div>

							<?
						}
						?>


					</div>

					<div class="row stats">

						<div class="col-md-3">
							<h1>#<? echo array_search(bp_displayed_user_id(),array_keys($lijst)) + 1; ?></h1>
							<p><img src="<?=get_template_directory_uri()?>/images/impactlijst.png">impactlijst</p>
							<!-- <i>Jouw positie</i> -->
							<!-- <i class="fa fa-fw fa-sort-down" style=" color:red;"></i><b style="color:red">-2</b>  -->
						</div>

						<div class="col-md-3">
							<h1>€<?php echo array_sum($mutatie_saldo);?></h1>
							<p><img src="<?=get_template_directory_uri()?>/images/impactsaldo.png">saldo <!-- saldo --></p>
							<!-- <i>Direct uit te betalen</i> -->
							<!-- <i class="fa fa-fw fa-sort-up" style=" color:#19d895;"></i><b style="color:#19d895">+173%</b> --> 
						</div>

						<div class="col-md-3">
							<h1><?php echo array_sum($mutatie_punten);?></h1>
							<p><img src="<?=get_template_directory_uri()?>/images/impactpunt.png">impactpunten</p>
							<!-- <i>Totale waarde</i> -->
							<!-- <i class="fa fa-fw fa-sort-up" style=" color:#19d895;"></i><b style="color:#19d895">+73%</b>  -->
						</div>

						<div class="col-md-3">
							<h1>€<!-- € --> <?php echo array_sum($mutatie_zegels);?></h1>
							<p><img src="<?=get_template_directory_uri()?>/images/impactzegel.png">royalties <!-- royalties --></p>
							<!-- <i>Nodig om impactpunten uit te betalen</i> -->

							<!-- <i class="fa fa-fw fa-sort-up" style=" color:#19d895;"></i><b style="color:#19d895">+23%</b>  -->
						</div>

					</div>


					<? if (get_current_user_id() == bp_displayed_user_id() || get_current_user_id() == 1) { ?>

						<div id="dashboard">

							<Br>

							<? if (array_sum($mutatie_punten) == 0) {
								?>
								<div class="alert alert-info alert alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<i>Zet je saldo om naar impactpunten en verdien tot 10x zoveel via royalties. <a class="nav-link" href="" data-toggle="modal" data-target="#modal-converteren">Meer informatie</a></i>
								</div>
								<?
							}
							?>

							<? if (array_sum($mutatie_punten) > 0) {
								?>
								<div class="alert alert-info alert alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<i>Ontvang extra impactpunten door deze voor 1, 3 of 5 jaar vast te zetten. <a class="nav-link" href="" data-toggle="modal" data-target="#modal-ontgrendelen">Meer informatie</a></i>
								</div>
								<?
							}
							?>
							<br>

							<ul class="nav nav-tabs tabs center-pills">
								<li class="nav-item">
									<a class="nav-link" href="#verdienen">VERDIENEN</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#beheren">BEHEREN</a> 
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#inwisselen">VERZILVEREN</a>
								</li>
							</ul>

							<div id="verdienen">

								<div class="row">

									<div class="col-md-9 projects">
										<h2>
											<div class="icon">
												<img src="<?=get_template_directory_uri()?>/images/verdienen.png">
											</div>
										Jouw projecten</h2>
									</div>

									<div class="col-md-12 padding-left-20">

										<?
										$posts = get_posts( 
											array(
												'post_type' =>  'project',
												'posts_per_page' => -1
											)
										);

										$counter = 0;

										foreach ( $posts as $post ) {

											$coauthors = get_coauthors(); 

											foreach ( $coauthors as $coauthor ) {

												if ($coauthor->ID == bp_displayed_user_id() && $counter < 5) {
													if (get_field('status') == 'in uitvoering') {
														$counter++;

														if ($counter == 1) {
															?><table class="table">
																<thead>
																	<tr>
																		<th scope="col">#</th>
																		<th scope="col">Projectnaam</th>

																		<th scope="col">Vergoeding</th>
																		<th scope="col">Status</th>
																		<!-- <th scope="col">Aantal taken</th> -->
																		<th scope="col">Deadline</th>
																		<th scope="col">Declareren</th>
																	</tr>
																</thead>
																<tbody>
																	<?
																}

																echo '<tr><td>';
																echo $counter;
																echo '</td>';
																echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
																if(get_field('vergoeding_saldo')) {
																	echo '<td><img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactsaldo.png"> '.get_field('vergoeding_saldo').'</td>';
																}
																else if(get_field('vergoeding_punten')) {
																	echo '<td><img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactpunt.png"> '.get_field('vergoeding_punten').'</td>';
																}
																else {
																	echo '<td>nog in overleg</td>';
																}

																echo '<td>'.get_field('status').'</td>';
																echo '<td>'.get_field('deadline').'</td>';
																echo '<td>'.get_field('declaratie').'</td>';
																echo '</tr>';
															}

														}
													}

												}

												if ($counter == 0) {
													echo '<i>Je neemt op dit moment nog niet deel aan een project</i>';
												}
												else {
													?></tbody>
													</table><?
												}

												if ($counter == 5) {
													?><a class="float-right"  data-toggle="modal" data-target="#stemmingen">Bekijk alles ></a><?
												}

												?>


											</div>

										</div>

										<Br>

										<div class="row">

											<div class="col-md-12 projects">
												<h2>
													<div class="icon">
														<img src="<?=get_template_directory_uri()?>/images/freelanceprojecten.png">
													</div>
												Freelance projecten </h2>

												<?
												$posts = get_posts( 
													array(
														'post_type' =>  'project',
														'posts_per_page' => -1
													)
												);

												$counter = 0;

												foreach ( $posts as $post ) {

													if (get_field('inschrijving') && !get_field('vergoeding_punten') && $counter < 5) {
														if ($counter == 0) {
															?>
															<table class="table">
																<thead>
																	<tr>
																		<th scope="col">#</th>
																		<th scope="col">Omschrijving</th>
																		<th scope="col">Vergoeding</th>
																		<th scope="col">Status</th>
																		<th scope="col">Aantal taken</th>
																		<th scope="col">Nog beschikbaar</th>
																		<th scope="col">Inschrijving</th>
																	</tr>
																</thead>
																<tbody><?
															}
															$counter++;
															echo '<tr><td>';
															echo $counter;
															echo '</td>';
															echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
															echo '<td><img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactsaldo.png"> '.get_field('vergoeding_saldo').'</td>';
															echo '<td>'.get_field('status').'</td>';
															echo '<td>'.count(get_field('taken')).'</td>';

															$total = 0;

															if (have_rows('taken')):

																while (have_rows('taken')): the_row(); 
																	$value = get_sub_field('status');
																	if ($value == "beschikbaar") {
																		$total++;
																	};
																endwhile;
														//echo $total;
															endif;

															echo '<td>'.$total.'</td>';
															if(get_field('inschrijving') == 'besloten' || get_field('inschrijving') == 'gesloten') {
																echo '<td>'.get_field('inschrijving').'</td>';
															}
															else {
															//echo '<td><a href="#" class="badge badge-success">Inschrijven</a></td>';
																echo '<td>open</td>';
															}

															echo '</tr>';
														}

													}
													?>

													<? if ($counter == 0) {
														echo '<i>Er zijn op dit moment geen community opdrachten beschikbaar</i>';
													}
													else {
														?></tbody>
														</table><?
													}

													if ($counter == 5) {
														?><a class="float-right"  data-toggle="modal" data-target="#stemmingen">Bekijk alles ></a><?
													}

													?>

												</div>

											</div>

											<Br>

											<div class="row">

												<div class="col-md-9 projectes">
													<h2>
														<div class="icon">
															<img src="<?=get_template_directory_uri()?>/images/communityprojecten.png">
														</div>
													Community projecten</h2>
												</div>

												<div class="col-md-12 padding-left-20">
													<?
													$posts = get_posts( 
														array(
															'post_type' =>  'project',
															'posts_per_page' => -1
														)
													);

													$counter = 0;

													foreach ( $posts as $post ) {

														if (get_field('inschrijving') && get_field('vergoeding_punten') && $counter < 5) {

															if ($counter == 0) {
																?><table class="table">
																	<thead>
																		<tr>
																			<th scope="col">#</th>
																			<th scope="col">Omschrijving</th>
																			<th scope="col">Vergoeding</th>
																			<th scope="col">Status</th>
																			<th scope="col">Aantal taken</th>
																			<th scope="col">Nog beschikbaar</th>
																			<th scope="col">Inschrijving</th>
																		</tr>
																	</thead>
																	<tbody><?
																}
																$counter++;
																echo '<tr><td>';
																echo $counter;
																echo '</td>';
																echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
																echo '<td><img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactpunt.png"> '.get_field('vergoeding_punten').'</td>';
																echo '<td>'.get_field('status').'</td>';
																echo '<td>'.count(get_field('taken')).'</td>';

																$total = 0;
																if (have_rows('taken')):

																	while (have_rows('taken')): the_row(); 
																		$value = get_sub_field('status');
																		if ($value == "beschikbaar") {
																			$total++;
																		};
																	endwhile;
															//echo $total;
																endif;

																echo '<td>'.$total.'</td>';
																echo '<td>'.get_field('inschrijving').'</td>';
																echo '</tr>';
															}

														}

														if ($counter == 0) {
															echo '<i>Er zijn op dit moment geen community opdrachten beschikbaar</i>';
														}
														else {
															?></tbody>
															</table><?
														}

														if ($counter == 5) {
															?><a class="float-right"  data-toggle="modal" data-target="#stemmingen">Bekijk alles ></a><?
														}

														?>
													</div>

												</div>


											</div>

											<div id="beheren">

												<div class="row">

													<div class="col-md-7 projects progressie">
														<h2>
															<div class="icon">
																<img src="<?=get_template_directory_uri()?>/images/impactstijging.png" >
															</div>
															Progressie

														</h2>  
														<a class="impactlijst-ref">Bekijk jouw positie op de impactlijst ></a></h2>
														<?php //echo do_shortcode( '[visualizer id="7888"]' ); ?>
														<div id="curve_chart" style="width: 100%; height: 100%;"></div>

													</div>

													<div class="col-md-5" style=" padding-top:10px; ">
														<h2>Vergroot jouw impact</h2>
														<hr>
														<a class="nav-link" href="" data-toggle="modal" data-target="#modal-opdrachten">
															<div class="icon">
																<img src="<?=get_template_directory_uri()?>/images/verdienen.png">
															</div>
															Voer opdrachten uit >
														</a>
														<hr>
														<!-- <i class="fa fa-fw fa-sort-up" style=" color:#19d895;"></i><b style="color:#19d895">+73%</b>  -->
														<a class="nav-link" href="" data-toggle="modal" data-target="#modal-voorstellen">
															<div class="icon">
																<img src="<?=get_template_directory_uri()?>/images/voorstellen.png">
															</div>
															Schrijf een projectvoorstel >
														</a>
														<hr>
														<!-- <i class="fa fa-fw fa-sort-up" style=" color:#19d895;"></i><b style="color:#19d895">+73%</b>  -->
														<a class="nav-link" href="" data-toggle="modal" data-target="#modal-donereninvesteren">
															<div class="icon">
																<img src="<?=get_template_directory_uri()?>/images/bijdragen.png">
															</div>
															Doneer/investeer >
														</a>
														<hr>
														<!-- <i class="fa fa-fw fa-sort-up" style=" color:#19d895;"></i><b style="color:#19d895">+73%</b> --> 
														<a class="nav-link" href="" data-toggle="modal" data-target="#modal-converteren">
															<div class="icon">
																<img src="<?=get_template_directory_uri()?>/images/converteersaldo.png">
															</div>
															Converteer saldo >
														</a>
														<!-- <i class="fa fa-fw fa-sort-up" style=" color:#19d895;"></i><b style="color:#19d895">+200%</b>  -->
														<hr>
														<a class="nav-link" href="" data-toggle="modal" data-target="#modal-ontgrendelen">
															<div class="icon">
																<img src="<?=get_template_directory_uri()?>/images/unlockpunten.png">
															</div>
															Ontgrendel impactpunten >
														</a>
														<!-- <i class="fa fa-fw fa-sort-up" style=" color:#19d895;"></i><b style="color:#19d895">+500%</b>  -->
													</div>

												</div>


												<br>

												<div class="row">

													<div class="col-md-10 padding-left-20">
														<h2>
															<div class="icon">
																<img src="<?=get_template_directory_uri()?>/images/zeggenschap.png">
															</div>
														Oefen zeggenschap uit</h2>
													</div>


													<div class="col-md-12 padding-left-20">

														<?

														$posts = get_posts( 
															array(
																'post_type' =>  'project',
																'posts_per_page' => -1
															)
														);

														$counter = 0;

														foreach ( $posts as $post ) {

															if (get_field('status') == "in stemming" && $counter < 5) {
																if ($counter == 0) {
																	?><table class="table">
																		<thead>
																			<tr>
																				<tr>
																					<th scope="col">#</th>
																					<th scope="col">Projectvoorstel</th>
																					<!-- <th scope="col">Benodigd budget</th> -->
																					<th scope="col">Status</th>
																					<th scope="col">Stemperiode</th>
																					
																				</tr>
																			</tr>
																		</thead>
																		<tbody>
																			<?
																		}
																		$counter++;
																		echo '<tr><td>';
																		echo $counter;
																		echo '</td>';
																		echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
																		echo '<td>'.get_field('status').'</td>';
																		echo '<td>'.get_field('stemperiode').'</td>';
																		echo '</tr>';

																	}
																}

																if ($counter > 0) {
																	?>
																</tbody>
																</table><?
															}
															else {
																echo '<i>Er zijn nog geen stemmingen beschikbaar</i>';
															}
															if ($counter == 5) {
																?><a class="float-right"  data-toggle="modal" data-target="#stemmingen">Bekijk alles ></a><?
															}

															?>

														</div>

													</div>

													<br>

													<div class="row">

														<div class="col-md-10 padding-left-20">
															<h2>
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/impactlijst.png">
																</div>
															Jouw naamsvermeldingen</h2>
														</div>

														<br>

														<div class="col-md-12 padding-left-20">

															<?
															$posts = get_posts( 
																array(
																	'post_type' =>  'post',
																	'post_status' => 'publish',
																	'posts_per_page' => -1
																)
															);

															$counter = 0;


															foreach ( $posts as $post ) {

																$coauthors = get_coauthors(); 

																foreach ( $coauthors as $coauthor ) {

																	if ($coauthor->ID == bp_displayed_user_id() && $counter < 5) {
																		$counter++;

																		if ($counter == 1) {
																			?><table class="table">

																				<thead>
																					<tr>
																						<th scope="col">#</th>
																						<th scope="col">Link</th>
																					</tr>
																				</thead>
																				<tbody><?
																			}

																			echo '<tr><td>';
																			echo $counter;
																			echo '</td>';
																			echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . '</a></td>';
																			echo '</tr>';
																		}
																	}

																}

																if ($counter > 0) {
																	?>

																</tbody>
																</table><?
															}
															else {
																echo '<i>Je hebt nog niets bijgedragen wat leidt tot een naamsvermelding</i>';
															}
															if ($counter == 5) {
																?><a class="float-right" data-toggle="modal" data-target="#modal-naamsvermelding">Bekijk alles ></a><?
															}


															?>

														</div>
													</div>

													<Br>

													<div class="row">

														<div class="col-md-9 projects">
															<h2>
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/voorstellen.png">
																</div>
															Jouw projectvoorstellen</h2>
														</div>
														<div class="col-md-3 padding-left-20">
															<button type="button" class="btn btn-primary nav-link" data-toggle="modal" data-target="#modal-voorstellen" style="margin-top:-10px;">
																Voorstel schrijven
															</button>
														</div>

														<div class="col-md-12 padding-left-20">

															<?
															$posts = get_posts( 
																array(
																	'post_type' =>  'project',
																	'posts_per_page' => -1
																)
															);

															$counter = 0;

															foreach ( $posts as $post ) {

																$coauthors = get_coauthors(); 

																foreach ( $coauthors as $coauthor ) {

																	if ($coauthor->ID == bp_displayed_user_id() && $counter < 5) {
																		$counter++;

																		if ($counter == 1) {
																			?>
																			<table class="table">
																				<thead>
																					<tr>
																						<th scope="col">#</th>
																						<th scope="col">Omschrijving</th>
																						<!-- <th scope="col">Benodigd budget</th> -->
																						<th scope="col">Status</th>
																						<th scope="col">Afgerond</th>
																					</tr>
																				</thead>
																				<tbody><?
																			}

																			echo '<tr><td>';
																			echo $counter;
																			echo '</td>';
																			echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
																			//echo '<td>€ '.get_field('budget').'</td>';
																			echo '<td>'.get_field('status').'</td>';
																			echo '<td>'.get_field('afgerond').'</td>';
																			echo '</tr>';
																		}
																	}

																}

																if ($counter > 0) {
																	?>
																</tbody>
																</table><?
															}
															else {
																echo '<i>Je hebt nog geen projectvoorstellen geschreven</i>';
															}
															if ($counter == 5) {
																?><a class="float-right"  data-toggle="modal" data-target="#modal-voorstellen">Bekijk alles ></a><?
															}

															?>

														</div>

													</div>


												</div>

												<div id="inwisselen">

													<div class="row totals">

														<div class="col-md-4 padding-left-20">
															<h3>Te verkrijgen impactpunten</h3>
														</div>
														<div class="col-md-3 padding-left-20">
															<h2>
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/impactlijst.png">
																</div>
																<?php echo array_sum($mutatie_saldo)*2;?>
															</h2>
														</div>
														<div class="col-md-5 padding-left-20">
															<a class="nav-link" href="" data-toggle="modal" data-target="#modal-converteren">
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/converteersaldo.png">
																</div>
																Converteer saldo >
															</a>
														</div>

														<hr>

														<div class="col-md-4 padding-left-20">
															<h3>Uitbetaalbaar saldo</h3>
														</div>
														<div class="col-md-3 padding-left-20">
															<h2>
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/impactsaldo.png">
																</div>
																€<?php echo array_sum($mutatie_saldo); ?>
															</h2>
														</div>
														<div class="col-md-5 padding-left-20">

															<a class="nav-link" href="" data-toggle="modal" data-target="#modal-uitbetalensaldo">
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/uitbetalensaldo.png">
																</div>
																Vraag uitbetaling saldo aan >
															</a>
														</div>

														<hr>

														<div class="col-md-4 padding-left-20">
															<h3>Te verdienen impactpunten</h3>
														</div>
														<div class="col-md-3 padding-left-20">
															<h2>
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/impactstijging.png">
																</div>
																<?php echo array_sum($mutatie_punten)*9;?>
															</h2>
														</div>
														<div class="col-md-5 padding-left-20">
															<a class="nav-link" href="" data-toggle="modal" data-target="#modal-ontgrendelen">
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/unlockpunten.png">
																</div>
																Ontgrendel impactpunten >
															</a>
														</div>

														<hr>

														<div class="col-md-4 padding-left-20">
															<h3>Beschikbare impactpunten</h3>
														</div>
														<div class="col-md-3 padding-left-20">
															<h2>
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/impactpunt.png">
																</div>
																<?php echo array_sum($mutatie_punten);?>
															</h2>
														</div>
														<div class="col-md-5 padding-left-20">
															<a class="nav-link" href="" data-toggle="modal" data-target="#modal-inwisselenpunten">
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/inwisselenpunten.png">
																</div>
																Wissel punten in >
															</a>
														</div>

														<hr>

														<div class="col-md-4 padding-left-20">
															<h3>Uitbetaalbare impactpunten</h3>
														</div>
														<div class="col-md-3 padding-left-20">
															<h2>
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/impactzegel.png">
																</div>
																€<?php echo array_sum($mutatie_zegels);?>
															</h2>
														</div>
														<div class="col-md-5 padding-left-20">
															<a class="nav-link" href="" data-toggle="modal" data-target="#modal-uitbetalenpunten">
																<div class="icon">
																	<img src="<?=get_template_directory_uri()?>/images/uitbetalenpunten.png">
																</div>
																Vraag uitbetaling impactpunten aan >
															</a>
														</div>

													</div>

													<br>

													<div class="row">

														<div class="col-md-10 padding-left-20">
															<h2>Transacties</h2>
														</div>

														<div class="col-md-12 padding-left-20">


															<? $counter = 0;

															if( have_rows('transacties', 'user_'.bp_displayed_user_id())):

																while( have_rows('transacties', 'user_'.bp_displayed_user_id()) ) : the_row();

																	$counter++;

																	if ($counter == 1) {
																		?><table class="table datatable">
																			<thead>
																				<tr>
																					<th scope="col">#</th>
																					<th scope="col">Datum</th>
																					<th scope="col">Mutatie</th>
																					<th scope="col">Handeling</th>
																					<!-- <th scope="col">Details</th> -->
																				</tr>
																			</thead>
																			<tbody><?
																		}

																		echo "<tr>";
																		echo "<td>".$counter."</td>";

																		echo "<td>".get_sub_field('tijdstempel')."</td>";

																		if (get_sub_field('impactsaldo')) {
																			echo "<td>impactsaldo ".get_sub_field('impactsaldo');

																			if (get_sub_field('impactpunten')) {
																				echo ", impactpunten ".get_sub_field('impactpunten')."</td>";
																			}

																			echo "</td>";
																		}
																		else if (get_sub_field('impactpunten')) {
																			echo "<td>impactpunten ".get_sub_field('impactpunten')."</td>";
																		}
																		else if (get_sub_field('impactzegels')) {
																			echo "<td>impactzegels ".get_sub_field('impactzegels')."</td>";
																		}

																		echo "<td>".get_sub_field('handeling')."</td>";

																						//echo "<td><a>Bekijk ></a></td>";

																		echo "</tr>";

																	endwhile;

																endif; 

																if ($counter > 0) {
																	?>

																</tbody>
																</table><?
															}
															else {
																echo '<i>Er zijn geen transacties gevonden</i>';
															}

															?>

														</div>

													</div>

												</div>

											</div>

											<br><br>

											<div id="informatie">


												<div class="row">
													<div class="col-md-6">
														<h2>Het Explor Onderwijs impactprogramma</h2>
														<br><br>

														<img src="<?=get_template_directory_uri()?>/images/impactprogramma.png" />
													</div>
													<div class="col-md-3 legenda">
														<ol>

															<br><b>FINANCIERING EN ONTWIKKELING</b>

															<li>Donaties en investeringen<br><a data-toggle="modal" data-target="#modal-donereninvesteren">Draag bij ></a></li>
															<li>Impactfonds<br><a data-toggle="modal" data-target="#modal-impactfonds">Bekijk overzicht ></a></li>
															<li>Freelance projecten<br><a data-toggle="modal" data-target="#modal-freelanceprojecten">Draag bij ></a></li>
															<li>Impactsaldo uitbetalen<br><a data-toggle="modal" data-target="#modal-uitbetalensaldo">Vraag uitbetaling aan ></a></li>
															<li>Verkrijg impactpunten<br><a data-toggle="modal" data-target="#modal-converteren">Converteer saldo ></a></li>
															<li>Projectvoorstellen<br><a data-toggle="modal" data-target="#modal-voorstellen">Draag bij ></a></li>
															<li>Stemmingen <br><a data-toggle="modal" data-target="#modal-stemmingen">Oefen zeggenschap uit ></a></li>
															<li>Projectfinanciering<br><a data-toggle="modal" data-target="#modal-voorstelindienen">Dien projectvoorstel in ></a></li>
															<li>Community projecten<br><a data-toggle="modal" data-target="#modal-communityprojecten">Draag bij ></a></li>
															<li>Impactpunten inwisselen<br><a data-toggle="modal" data-target="#modal-inwisselenpunten">Verzilver impactpunten ></a></li>

														</ol>

													</div>

													<div class="col-md-3 legenda">

														<ol start="11">
															<br><b>OPBRENGSTEN</b>
															<li>Product- en dienstverkoop<br><a href="https://helpmijslagen.nl/">Bezoek webshop ></a></li>
															<li>Jaaroverzicht <br><a data-toggle="modal" data-target="#modal-jaaroverzicht">Bekijk resultaten ></a></li>

															<br><b>VERDELING EN BELONING </b>
															<li>Te verdelen<br><a data-toggle="modal" data-target="#modal-opbrengsten">Bekijk opbrengsten ></a></li>
															<li>Naamsvermelding <br><a data-toggle="modal" data-target="#modal-naamsvermelding">Bekijk vermeldingen > </a></li>
															<li>Royalty verdeling <br><a data-toggle="modal" data-target="#modal-royalties"> Meer informatie > </a></li>
															<li>Impactpunten uitbetalen <br><a data-toggle="modal" data-target="#modal-uitbetalenpunten">Vraag uitbetaling aan ></a></li>
															<li>Betrokkenheid vergroten <br><a data-toggle="modal" data-target="#modal-ontgrendelen">Ontgrendel impactpunten ></a></li>
															<li>Ontvangsten vergroten <br><a class="impactlijst-ref">Bekijk impactlijst ></a></li>
															<li>Impactpunten uitbetalen <br><a data-toggle="modal" data-target="#modal-uitbetalenpunten">Vraag uitbetaling aan ></a></li>
														</ol>

													</div>
												</div>
												
											</div>


											<div id="impactlijst">

												<div class="row">
													<div class="col-md-12">
														<h2>Impactlijst</h2>

														<table class="table datatable">
															<thead>
																<tr>
																	<th scope="col">Positie</th>
																	<th scope="col">Naam</th>
																	<th scope="col">Totaal verdiende impactpunten</th>
																	<th scope="col">Actief aandeel</th>
																</tr>
															</thead>
															<tbody>

																<? $counter = 0;
																foreach ($lijst as $key => $value) {
																	$counter++;
																	echo '<tr><td>';
																	echo $counter;
																	echo '</td><td>';
																	echo bp_core_get_userlink($key);
																	echo '</td><td><img src="'.get_template_directory_uri().'/images/impactpunt.png"> ';
																	echo $value;
																	echo '</td><td>';
																							echo round($value/array_sum($lijst)*100,1);
																	echo '%</td></tr>';

																}
																?>

															</tbody>
														</table>

													</div>
												</div>

											</div>

										<? } ?>

										<? if (get_current_user_id() != bp_displayed_user_id() && get_current_user_id() != 1) { echo '<br><br>'; } ?>

										<div id="profiel">

											<div class="row">
												<div class="col-md-6">
													<h2>Recente activiteit</h2>
													<?
								// WP_Comment_Query arguments
													$args = array (
														'user_id'        => bp_displayed_user_id(),
								    // 'post_status'    => 'approve',
														'number'         => '5',
													);

								// The Comment Query
													$comments = new WP_Comment_Query;
													$comments = $comments->query( $args );
													$counter=0;

								// The Comment Loop
													if ( $comments ) {

														?><table class="table">
															<thead>
																<tr>
																	<th scope="col">#</th>
																	<th scope="col">Post</th>
																	<th scope="col">Status</th>
																	<th scope="col">Vergoeding</th>
																</tr>
															</thead>
															<tbody><?

															foreach ( $comments as $c ) {
																$counter++;

																$output.= "<tr>\n";
																$output.= '<td>'.$counter.'</td><td>';
																$output.= '<a href="'.get_comment_link( $c->comment_ID ).'">';
																$output.= get_the_title($c->comment_post_ID);
								        //$output.= '</a>, Posted on: '. mysql2date('m/d/Y', $c->comment_date, $translate);
																$output.= "</td>";
																$output.= "<td>";
																if(get_field('status', $c)){$output.= get_field('status', $c); }else { $output.= 'in review'; }
																$output.= "</td><td>";
																$output.= get_field('vergoeding', $c);
																$output.= "</td>\n";
																$output.= '</tr>';
															}

															echo $output;

															?></tbody>
															</table><?
														} else {
															echo '<i>Er is geen recente activiteit te vinden voor deze gebruiker</i>';
														}

														?>
														<!-- <i>Er is nog geen activiteit </i> -->
													</div>

													
													<div class="col-md-6 padding-left-20">

														<h2>
															<div class="icon">
																<img src="<?=get_template_directory_uri()?>/images/verdienen.png">
															</div>
														Actieve projecten</h2>

														<?
														$posts = get_posts( 
															array(
																'post_type' =>  'project',
																'posts_per_page' => -1
															)
														);

														$counter = 0;

														foreach ( $posts as $post ) {

															$coauthors = get_coauthors(); 

															foreach ( $coauthors as $coauthor ) {

																if ($coauthor->ID == bp_displayed_user_id() && $counter < 5) {
																	if (get_field('status') == 'in uitvoering') {
																		$counter++;

																		if ($counter == 1) {
																			?><table class="table">
																				<thead>
																					<tr>
																						<th scope="col">#</th>
																						<th scope="col">Projectnaam</th>
																					</tr>
																				</thead>
																				<tbody>
																					<?
																				}

																				echo '<tr><td>';
																				echo $counter;
																				echo '</td>';
																				echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
																				

																				echo '</tr>';
																			}

																		}
																	}

																}

																if ($counter == 0) {
																	echo '<i>Je neemt op dit moment nog niet deel aan een project</i>';
																}
																else {
																	?></tbody>
																	</table><?
																}

																if ($counter == 5) {
																	?><a class="float-right"  data-toggle="modal" data-target="#stemmingen">Bekijk alles ></a><?
																}

																?>


															</div>

														</div>

														<br>

														<div class="row">

															<div class="col-md-7 padding-left-20">

																<h2>
																	<div class="icon">
																		<img src="<?=get_template_directory_uri()?>/images/voorstellen.png">
																	</div>
																Voltooide projecten</h2>

																<?
																$posts = get_posts( 
																	array(
																		'post_type' =>  'project',
																		'posts_per_page' => -1
																	)
																);

																$counter = 0;

																foreach ( $posts as $post ) {

																	$coauthors = get_coauthors(); 

																	foreach ( $coauthors as $coauthor ) {

																		if ($coauthor->ID == bp_displayed_user_id()) {
																			$counter++;

																			if ($counter == 1) {
																				?>
																				<table class="table">
																					<thead>
																						<tr>
																							<th scope="col">#</th>
																							<th scope="col">Omschrijving</th>


																							<th scope="col">Status</th>
																							<th scope="col">Opgeleverd</th>
																						</tr>
																					</thead>
																					<tbody><?
																				}

																				echo '<tr><td>';
																				echo $counter;
																				echo '</td>';
																				echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
																				echo '<td>'.get_field('status').'</td>';
																				echo '<td>'.get_field('afgerond').'</td>';
																				echo '</tr>';
																			}
																		}

																	}

																	if ($counter > 0) {
																		?>
																	</tbody>
																	</table><?
																}
																else {
																	echo '<i>Je hebt nog geen projectvoorstellen geschreven</i>';
																}
																if ($counter == 5) {
																	?><a class="float-right"  data-toggle="modal" data-target="#modal-voorstellen">Bekijk alles ></a><?
																}

																?>

															</div>


															<div class="col-md-5">
																<h2>Track-record</h2>
																<div id="curve_chart_open"></div>
															</div>

														</div>



													</div>


													<? if (get_current_user_id() == bp_displayed_user_id() || get_current_user_id() == 1) { ?>

														<div id="administratie">



															<div class="row">

																<div class="col-md-12 padding-left-20">
																	<h2>Jouw nota's</h2>

																	<?
																	$posts = get_posts( 
																		array(
																			'post_type' =>  'nota',
																			'author' => get_current_user_id(),
																			'posts_per_page' => -1
																		)
																	);

																	$counter = 0;

																	foreach ( $posts as $post ) {

																		if ($counter == 0) {
																			?>
																			<table class="table datatable">
																				<thead>
																					<tr>
																						<th scope="col">#</th>
																						<th scope="col">Omschrijving</th>
																						<th scope="col">Ingediend</th>
																						<th scope="col">Status</th>
																						<th scope="col">Uitbetaald</th>

																					</tr>
																				</thead>
																				<tbody><?
																			}
																			$counter++;
																			echo '<tr><td>';
																			echo $counter;
																			echo '</td>';
																			echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
																			echo '<td>'.get_field('datum').'</td>';
																			echo '<td>'.get_field('status').'</td>';
																			echo '<td>'.get_field('uitbetaald').'</td>';
																			echo '</tr>';
																		}


																		?>

																		<? if ($counter == 0) {
																			echo '<i>Er zijn nog geen notas gemaakt</i>';
																		}
																		else {
																			?>

																		</tbody>
																		</table><?
																	}

																	?>

																</div>	

															</div>


														</div>

														<div id="accountgegevens">



															<div class="row">
																<div class="col-md-6">
																	<h2>Algemene gegevens</h2>
																	<?
																	$options = array(
																		'fields' => array('email', 'naamsvermelding', 'beroep'),
																		'post_id' => 'user_'.get_current_user_id(),
																	);
																	acf_form($options); ?>
																</div>

																<div class="col-md-6">
																	<h2>Uitbetaal gegevens</h2>
																	<?
																	$options = array(
																		'fields' => array('voornaam', 'achternaam', 'bedrijfsnaam', 'bankrekening'),
																		'post_id' => 'user_'.get_current_user_id(),
																	);
																	acf_form($options); 

															//echo do_shortcode('[advanced_form form="form_5dea376458ec1"]');
																	?>
																</div>

															</div>




														</div>

													<? } ?>
													<br><br>

												</div>
											</div>
										</div>

									</main>

									<?php get_footer(); ?>


									<!-- Modal --> 
									<div class="modal fade" id="modal-opdrachten" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Voer opdrachten uit</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">

													<?

													$posts = get_posts( 
														array(
															'post_type' =>  'project',
															'posts_per_page' => -1
														)
													);

													$counter = 0;

													foreach ( $posts as $post ) {

														if (get_field('inschrijving')) {
															if ($counter == 0) {
																?>
																<table class="table datatable" id="alle-projecten">
																	<thead>
																		<tr>
																			<th scope="col">#</th>
																			<th scope="col">Omschrijving</th>
																			<th scope="col">Vergoeding</th>
																			<th scope="col">Status</th>
																			<th scope="col">Aantal taken</th>
																			<th scope="col">Nog beschikbaar</th>
																			<th scope="col">Inschrijving</th>
																		</tr>
																	</thead>
																	<tbody><?
																}
																$counter++;
																echo '<tr><td>';
																echo $counter;
																echo '</td>';
																echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
																if (get_field('vergoeding_saldo')) {
																	echo '<td><img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactsaldo.png"> '.get_field('vergoeding_saldo').'</td>';
																}
																else {
																	echo '<td><img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactpunt.png"> '.get_field('vergoeding_punten').'</td>';
																}
																echo '<td>'.get_field('status').'</td>';
																echo '<td>'.count(get_field('taken')).'</td>';

																$total = 0;

																if (have_rows('taken')):

																	while (have_rows('taken')): the_row(); 
																		$value = get_sub_field('status');
																		if ($value == "beschikbaar") {
																			$total++;
																		};
																	endwhile;
															//echo $total;
																endif;

																echo '<td>'.$total.'</td>';
																if(get_field('inschrijving') == 'besloten' || get_field('inschrijving') == 'gesloten') {
																	echo '<td>'.get_field('inschrijving').'</td>';
																}
																else {
									//echo '<td><a href="#" class="badge badge-success">Inschrijven</a></td>';
																	echo '<td>open</td>';
																}

																echo '</tr>';
															}

														}
														?>
													</tbody>
												</table>

											</div>

											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
											</div>
										</div>
									</div>
								</div>

								<!-- Modal --> 
								<div class="modal fade" id="modal-voorstellen" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Schrijf projectvoorstel</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<p>Heb je zelf een goed idee? Schrijf er een projectvoorstel van en dien het in. Bij goedkeuring krijg je financiering voor de uitvoering hiervan. Let op dat het schrijven 10 impactpunten kost en om het voorstel in te kunnen dienen kost het je 20 impactpunten.</p>
												<br>

												<? if (array_sum($mutatie_punten) > 10 ) { ?> 
													<p>Met je huidige royalties kun je maximaal <?php echo array_sum($mutatie_zegels); ?> impactpunten uitbetalen. De waarde hiervan is €<?php echo array_sum($mutatie_zegels); ?>.</p>
													<br><form method="post"> 
														<input name="titel" placeholder="Vul een projectitel in" required type="text" />
														<br>
														<input type="submit" name="create" class="btn" value="Maak project aan"/> 
													</form> 
												<? } 
												else { echo '<i>Je hebt niet voldoende impactpunten om een project te beginnen.</i>'; } ?>



												

				<?php //echo do_shortcode( '[wpuf_form id="7877"]' );

				// acf_form(array(
				// 	'post_id'		=> 'new_post',
				// 	'post_title'	=> true,
				// 	'post_content'	=> false,
				// 	'new_post'		=> array(
				// 		'post_type'		=> 'project',
				// 		'post_status'	=> 'publish'
				// 	),
				// 	'fields'        => array(''),
				// 	'return' => '%post_url%',
				// 	'submit_value'   => __( "Start met schrijven", 'acf' ),
				// ));

				if(isset($_POST['create'])) { 

					$post_data = array(
						'post_title' => $_POST["titel"],
        					'post_status' => 'publish', // Automatically publish the post.
        					'post_author' => get_current_user_id(),
        					'post_type' => 'project' 
        				);
					
					$row = array(
						'tijdstempel'	=> date("d-m-Y"),
						'impactpunten'	=> '-10',
						'handeling'	    => 'inwisseling'
					);

					add_row('transacties', $row, 'user_'.get_current_user_id());

					$link = wp_insert_post( $post_data);
					wp_redirect(get_permalink($link));
				} 

				?>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal --> 
<div class="modal fade" id="modal-donereninvesteren" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Doneer/investeer</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Wil je doneren of investeren? Neem dan contact op met info@exploronderwijs.nl. In de toekomst zal hier een aparte pagina voor opgezet worden.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal --> 
<div class="modal fade" id="modal-converteren" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Verkrijg impactpunten</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Om je impact te vergroten kun je je impactsaldo omzetten naar impactpunten. Je krijgt hiervoor het volgende terug: 
					<ul>

						<li>Een dubbel aantal impactpunten</li>
						<li>Meedeling in opbrengsten</li>
						<li>Naamsvermelding bij bijdragen</li>
						<li>Inwisselmogelijkheden voor producten en diensten</li>
						<li>Meer invloed en stemrechten</li>
						<li>Mogelijkheid om projectvoorstellen in te dienen</li>

					</ul>
					<P>Let wel op dat je impactpunten alleen nog maar kunt uitbetalen door middel van verkregen royalties. Deze worden door het bedrijf verstrekt bij opbrengsten. Door je impactpunten later vast te zetten kun je tot 10x zoveel verdienen. Maar let op, als het bedrijf geen tot weinig winst maakt kan dit een aantal jaar duren.</P>

					<br>
					<? if (array_sum($mutatie_saldo) > 0 ) { ?> 
						<p>Met je huidige impactsaldo kun je maximaal <?php echo array_sum($mutatie_saldo)*2; ?> impactpunten ontvangen. </p>
						<br><form method="post"> <input id="bedrag" name="bedrag" placeholder="Vul een bedrag in" required type="number" value="" min="0" max="1000" onchange="$('#impactpunten').html($('#bedrag').val()*2)" style="width:50%;"/>
							<br><i>Hiermee ontvang je <span id="impactpunten">0</span> impactpunten</i>
							<input type="submit" name="converteer" class="btn" value="Bedrag converteren"/> </form> 
						<? } 
						else { echo '<i>Je hebt niet voldoende saldo om te converteren naar impactpunten.</i>'; } ?>

						<?
						if(isset($_POST['converteer'])) { 

							$bedrag = $_POST['bedrag'];

							$row = array(
								'tijdstempel'	=> date("d-m-Y"),
								'impactsaldo'	=> '-'.$bedrag,
								'impactpunten'	=> $bedrag*2,
								'handeling'	    => 'conversie'
							);

							$i = add_row('transacties', $row, 'user_'.get_current_user_id());

							if (get_field('impactlijst_totaal')) {
								$itotaal = get_field('impactlijst_totaal');
							}
							else {
								$itotaal = 0;
							}


						//update field impactlijst punten
							$itotaal = get_field('impactlijst_totaal');
							$itotaal = $itotaal + $bedrag*2;
							update_field('impactlijst_totaal', $itotaal, 'user_'.get_current_user_id());
							header('Location: '.$_SERVER['REQUEST_URI']);

						} 

						?>

					</div>
					<div class="modal-footer">

						<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
					</div>
				</div>
			</div>
		</div>


		<!-- Modal --> 
		<div class="modal fade" id="modal-ontgrendelen" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ontgrendel impactpunten</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Om je impact te vergroten kun je je impactpunten vastzetten voor een bepaalde tijd. Hiervoor krijg je  
							<ul>

								<li>20% extra bij vastzetten voor 1 jaar</li>
								<li>120% extra bij vastzetten voor 3 jaar</li>
								<li>500% extra bij vastzetten voor 5 jaar</li>

							</ul>
							<P>Let wel op de impactpunten die je vastzet gedurende deze tijd niet meedelen in de opbrengsten. Je ontvangt dus geen royalties.</P>

							<br>
							<? if (array_sum($mutatie_punten) > 0 ) { ?> 
								<p>Met je huidige impactpunten kun je maximaal <?php echo array_sum($mutatie_punten); ?> impactpunten vastzetten. </p>
								<br><form method="post"> 
									

									<input id="aantal" name="aantal" placeholder="Vul een aantal in" required type="number" value="" min="0" max="1000" onchange="if($('#jaren').html() == 1) { $('#impactpunten_extra').html($('#aantal').val()*1.2).toFixed(2); } else if($('#jaren').html() == 3) { $('#impactpunten_extra').html($('#aantal').val()*2.2).toFixed(2); } else if($('#jaren').html() == 5) { $('#impactpunten_extra').html($('#aantal').val()*5).toFixed(2); } " style="width:50%"/>
									<br><br>
									<p>Voor hoeveel jaar?</p>
									<input type="radio" name="jaren" value="1 jaar" onchange="$('#impactpunten_extra').html(($('#aantal').val()*1.2).toFixed(2)); $('#jaren').html('1');"> 1 jaar<br>
									<input type="radio" name="jaren" value="3 jaar" onchange="$('#impactpunten_extra').html(($('#aantal').val()*2.2).toFixed(2)); $('#jaren').html('3');"> 3 jaar<br>
									<input type="radio" name="jaren" value="5 jaar" onchange="$('#impactpunten_extra').html(($('#aantal').val()*5).toFixed(2)); $('#jaren').html('5');"> 5 jaar<br>
									<i>Na <span id="jaren">1</span> jaar heb je <span id="impactpunten_extra">0</span> impactpunten</i>
									<br>
									<input type="submit" class="btn" name="converteer" value="Punten vastzetten"/> </form> 
								<? } 
								else { echo '<i>Je hebt niet voldoende impactpunten om vast te zetten.</i>'; } ?>

								<?
								if(isset($_POST['converteer'])) { 

									$aantal = $_POST['aantal'];
									$jaren = $_POST['jaren'];

									$row = array(
										'tijdstempel'	=> date("d-m-Y"),
										'impactpunten_vastgezet' => $aantal,
										'handeling'	    => 'vergrendeling',
										'locktijd' => $jaren
									);

									$i = add_row('transacties', $row, 'user_'.get_current_user_id());

									//update field impactlijst punten
									if (get_field('impactlijst_totaal')) {
										$itotaal = get_field('impactlijst_totaal');
									}
									else {
										$itotaal = 0;
									}
									
//verhoudingen toevoegen
									if ($jaren == 1) {
										$itotaal = $itotaal + $aantal*1.2;
									}
									else if ($jaren == 3) {
										$itotaal = $itotaal + $aantal*2.2;
									}
									else if ($jaren == 5) {
										$itotaal = $itotaal + $aantal*5;
									}

									update_field('impactlijst_totaal', $itotaal, 'user_'.get_current_user_id());
									header('Location: '.$_SERVER['REQUEST_URI']);

									//TODO automatisch ontgrendelen na datum verstreken is

								} 

								?>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-uitbetalensaldo" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Betaal impactsaldo uit</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">

								<p>Let op dat wanneer je je impactsaldo laat uitbetalen het niet meer mogelijk is om impactpunten te krijgen. Ook doe je hiermee afstand van eventuele auteursrechten.</p>
								<br>

								<? if (array_sum($mutatie_saldo) > 0 ) { ?> 
									<p>Met je huidige impactsaldo kun je maximaal €<?php echo array_sum($mutatie_saldo); ?> uitbetalen. </p>
									<br><form method="post"> <input name="bedrag" placeholder="Vul een bedrag in" required type="number" value="" min="0" max="<? echo array_sum($mutatie_saldo);?>" style="width:50%;"/><br><input type="submit" class="btn" name="payout" value="Uitbetaling aanvragen"/> </form> 
								<? } 
								else { echo '<i>Je hebt niet voldoende saldo om uit te betalen.</i>'; } ?>

								<?php

								if(isset($_POST['payout'])) { 
									$post_data = array(
										'post_title' => 'Uitbetalingsverzoek ' . date("Y-m-d_H:i:s"),
        					'post_status' => 'publish', // Automatically publish the post.
        					'post_author' => get_current_user_id(),
        					'post_type' => 'nota' 
        				);
									$link = wp_insert_post( $post_data);
									update_field('bedrag', $_POST["bedrag"], $link);

									// the message
									$msg = "Verzoek ingediend\nBekijk via ".get_permalink($link);
									mail("nico@exploronderwijs.nl","Uitbetalingsverzoek",$msg);

						//$link = get_permalink( $post_data );
									wp_redirect(get_permalink($link));
								} 

								?>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-uitbetalenpunten" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Betaal impactpunten uit</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">

								<p>Let op dat wanneer je je impactpunten laat uitbetalen het hiermee niet meer mogelijk is mee te delen in de opbrengsten van het bedrijf. Ook doe je hiermee afstand van eventuele auteursrechten.</p>

								<br>

								<? if (array_sum($mutatie_zegels) > 0 ) { ?> 
									<p>Met je tot nu toe ontvangen royalties kun je maximaal <?php echo array_sum($mutatie_zegels); ?> impactpunten uitbetalen. De waarde hiervan is €<?php echo array_sum($mutatie_zegels); ?>.</p>
									<br><form method="post"> <input name="bedrag" placeholder="Vul een bedrag in" required type="number" value="" min="0" style="width:50%;" max="<? echo array_sum($mutatie_impactzegels);?>"/><br><input type="submit" class="btn" name="payout_points" value="Uitbetaling aanvragen"/> </form> 
								<? } 
								else { echo '<i>Je hebt niet voldoende royalties verdiend om je impactpunten uit te betalen.</i>'; } ?>

								<?php
								if(isset($_POST['payout_points'])) { 
									$post_data = array(
										'post_title' => 'Uitbetalingsverzoek ' . date("Y-m-d_H:i:s"),
        					'post_status' => 'publish', // Automatically publish the post.
        					'post_author' => get_current_user_id(),
        					'post_type' => 'nota' 
        				);
									$link = wp_insert_post( $post_data);
									update_field('bedrag', $_POST["bedrag"], $link);

									// the message
									$msg = "Verzoek ingediend\nBekijk via ".get_permalink($link);
									mail("nico@exploronderwijs.nl","Uitbetalingsverzoek",$msg);

									wp_redirect(get_permalink($link));
								} 
								?>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-inwisselenpunten" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Wissel punten in</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p>Willen je impactpunten in tegen producten en diensten van Explor Onderwijs. Deze optie komt binnenkort beschikbaar</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-impactfonds" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Hoogte impactfonds</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p>Het impactfonds wordt regelmatig aangevuld met 50% van de opbrengsten die Explor Onderwijs genereert. De hoogte van het impactfonds is momenteel €<? echo get_field('impactfonds', 'user_1'); ?>. </p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-opbrengsten" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Opbrengsten te verdelen</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p>50% van de opbrengsten die gegenereert worden worden verdeeld over de personen met impactpunten. De opbrengsten die nog verdeeld moeten worden €<? echo get_field('verdelen', 'user_1'); ?>. Hierbij geldt hoe meer impactpunten je hebt, hoe meer je zult ontvangen. <a style="color:blue" data-toggle="modal" data-target="#modal-royalties"> Meer informatie over de verdeling > </a></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-freelanceprojecten" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Alle freelance projecten</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<?

								$posts = get_posts( 
									array(
										'post_type' =>  'project',
										'posts_per_page' => -1
									)
								);

								$counter = 0;

								foreach ( $posts as $post ) {

									if (get_field('inschrijving') && !get_field('vergoeding_punten')) {
										if ($counter == 0) {
											?>
											<table class="table datatable">
												<thead>
													<tr>
														<th scope="col">#</th>
														<th scope="col">Omschrijving</th>
														<th scope="col">Vergoeding</th>
														<th scope="col">Status</th>
														<th scope="col">Aantal taken</th>
														<th scope="col">Nog beschikbaar</th>
														<th scope="col">Inschrijving</th>
													</tr>
												</thead>
												<tbody><?
											}
											$counter++;
											echo '<tr><td>';
											echo $counter;
											echo '</td>';
											echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
											echo '<td><img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactsaldo.png"> '.get_field('vergoeding_saldo').'</td>';
											echo '<td>'.get_field('status').'</td>';
											echo '<td>'.count(get_field('taken')).'</td>';

											$total = 0;

											if (have_rows('taken')):

												while (have_rows('taken')): the_row(); 
													$value = get_sub_field('status');
													if ($value == "beschikbaar") {
														$total++;
													};
												endwhile;
														//echo $total;
											endif;

											echo '<td>'.$total.'</td>';
											if(get_field('inschrijving') == 'besloten' || get_field('inschrijving') == 'gesloten') {
												echo '<td>'.get_field('inschrijving').'</td>';
											}
											else {
									//echo '<td><a href="#" class="badge badge-success">Inschrijven</a></td>';
												echo '<td>open</td>';
											}

											echo '</tr>';
										}

									}
									?>
								</tbody>
							</table>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal --> 
			<div class="modal fade" id="modal-communityprojecten" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Alle community projecten</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">

							<?
							$posts = get_posts( 
								array(
									'post_type' =>  'project',
									'posts_per_page' => -1
								)
							);

							$counter = 0;

							foreach ( $posts as $post ) {

								if (get_field('inschrijving') && get_field('vergoeding_punten')) {
									if ($counter == 0) {
										?>
										<table class="table datatable">
											<thead>
												<tr>
													<th scope="col">#</th>
													<th scope="col">Omschrijving</th>
													<th scope="col">Vergoeding</th>
													<th scope="col">Status</th>
													<th scope="col">Aantal taken</th>
													<th scope="col">Nog beschikbaar</th>
													<th scope="col">Inschrijving</th>
												</tr>
											</thead>
											<tbody><?
										}
										$counter++;
										echo '<tr><td>';
										echo $counter;
										echo '</td>';
										echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
										echo '<td><img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactpunt.png"> '.get_field('vergoeding_punten').'</td>';
										echo '<td>'.get_field('status').'</td>';
										echo '<td>'.count(get_field('taken')).'</td>';

										$total = 0;

										if (have_rows('taken')):

											while (have_rows('taken')): the_row(); 
												$value = get_sub_field('status');
												if ($value == "beschikbaar") {
													$total++;
												};
											endwhile;
														//echo $total;
										endif;

										echo '<td>'.$total.'</td>';
										if(get_field('inschrijving') == 'besloten' || get_field('inschrijving') == 'gesloten') {
											echo '<td>'.get_field('inschrijving').'</td>';
										}
										else {
								//echo '<td><a href="#" class="badge badge-success">Inschrijven</a></td>';
											echo '<td>open</td>';
										}

										echo '</tr>';
									}

								}
								?>
							</tbody>
						</table>



					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal --> 
		<div class="modal fade" id="modal-stemmingen" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Alle stemmingen</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Voordat projecten uitgevoerd worden door freelancers of de community wordt vaak hierover gestemd. Hieronder een overzicht van alle stemmingen die er zijn of zijn geweest. In de toekomst is het mogelijk om over meer zaken te stemmen. Hierbij geldt dat elke impactpunt een stem vertegenwoordigt. </p>
						<?

						$posts = get_posts( 
							array(
								'post_type' =>  'project',
								'posts_per_page' => -1
							)
						);

						$counter = 0;

						foreach ( $posts as $post ) {

							if (get_field('status') == "in stemming") {
								if ($counter == 0) {
									?>
									<table class="table">
										<thead>
											<tr>
												<tr>
													<th scope="col">#</th>
													<th scope="col">Projectvoorstel</th>
													<!-- <th scope="col">Benodigd budget</th> -->
													<th scope="col">Stemperiode</th>
													<th scope="col">Status</th>
												</tr>
											</tr>
										</thead>
										<tbody>
											<?
										}
										$counter++;
										echo '<tr><td>';
										echo $counter;
										echo '</td>';
										echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . ' ></a></td>';
										echo '<td>'.get_field('stemperiode').'</td>';
										echo '<td>'.get_field('status').'</td>';
										echo '</tr>';

									}
								}

								if ($counter > 0) {
									?></tbody></table><?
								}
								else {
									echo '<i>Er zijn nog geen stemmingen beschikbaar</i>';
								}

								?>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-voorstelindienen" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Projectvoorstel indienen</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p>Wanneer je een projectvoorstel geschreven heb kan je die indienen. De community gaat dan stemmen. Deze optie komt later beschikbaar.</p>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-jaaroverzicht" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Jaaroverzicht</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p>De totale opbrengsten dit jaar zijn €<? echo get_field('opbrengsten', 'user_1'); ?>. 50% hiervan is naar het impactfonds gegaan, de overige 50% is verdeeld over de personen op de impactlijst.</p>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal --> 
				<div class="modal fade" id="modal-royalties" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Royalty verdeling</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p>50% van de opbrengsten worden verdeeld over de personen op de impactlijst. De helft van dit bedrag wordt naar ratio verdeeld conform het aandeelpercentage achter zijn of haar naam. De andere helft wordt verdeeld over de personen waarvan hun bijdrage onderdeel was van de opbrengst. De verdeling wordt nu nog door het bedrijf zelf gedaan, later zal dit ook geautomatiseerd worden.</p>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>


				<!-- Modal --> <!-- TODO nachecken -->
				<div class="modal fade" id="modal-naamsvermelding" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Jouw naamsvermeldingen</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<?
								$posts = get_posts( 
									array(
												//'author' => 1, 
										'posts_per_page' => -1
									)
								);

								$counter = 0;

								foreach ( $posts as $post ) {

									$coauthors = get_coauthors(); 

									foreach ( $coauthors as $coauthor ) {

										if ($coauthor->ID == bp_displayed_user_id() ) {
											$counter++;
											echo '<tr><td>';
											echo $counter;
											echo '</td>';
											echo '<td><a href="' . get_permalink( $post->ID ) . '">' . get_the_title( $post->ID ) . '</a></td>';
											echo '</tr>';

										}
									}


								}

								?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
							</div>
						</div>
					</div>
				</div>

				<script>$('ul.tabs').each(function(){
  // For each set of tabs, we want to keep track of
  // which tab is active and its associated content
  var $active, $content, $links = $(this).find('a');

  // If the location.hash matches one of the links, use that as the active tab.
  // If no match is found, use the first link as the initial active tab.
  $active = $($links.filter('[href="'+location.hash+'"]')[1] || $links[1]);
  $active.addClass('active');

  $content = $($active[0].hash);

  // Hide the remaining content
  $links.not($active).each(function () {
  	$(this.hash).hide();
  });

  // Bind the click event handler
  $(this).on('click', 'a', function(e){
    // Make the old tab inactive.
    $active.removeClass('active');
    $content.hide();

    // Update the variables with the new link and content
    $active = $(this);
    $content = $(this.hash);

    // Make the tab active.
    $active.addClass('active');
    $content.show();

    // Prevent the anchor's default click action
    e.preventDefault();
});
});</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
	$('.impactlijst-ref').click(function(event) {
		event.preventDefault();
		$('#dashboard').hide();
		$('#informatie').hide();
		$('#impactlijst').show();
		$('#profiel').hide();
		$('#accountgegevens').hide();
		$('#dashboard-link').removeClass('active');
		$('#informatie-link').removeClass('active');
		$('#impactlijst-link').addClass('active');
		$('#profiel-link').removeClass('active');
		$('#accountgegevens-link').removeClass('active');
	});
</script>

<script type="text/javascript">
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {

		var years = [<?php foreach ($tijd as $t) { echo "'".$t . "',"; } ?>];
		var impactsaldo = [<?php $m_s_tot = 0; foreach ($mutatie_saldo as $m_s) { $m_s_tot += $m_s; echo $m_s_tot . ","; } ?>];
		var impactpunten = [<?php $m_p_tot = 0; foreach ($mutatie_punten as $m_p) { $m_p_tot += $m_p; echo $m_p_tot . ","; } ?>];
		var impactzegels = [<?php $m_z_tot = 0; foreach ($mutatie_zegels as $m_z) { $m_z_tot += $m_z; echo $m_z_tot . ","; } ?>];

		var data = new google.visualization.DataTable();
		data.addColumn('string', 'years');
		data.addColumn('number', 'saldo');
		data.addColumn('number', 'impactpunten');
		data.addColumn('number', 'royalties');

		for(i = 0; i < years.length; i++) {
			data.addRow([years[i], impactsaldo[i], impactpunten[i], impactzegels[i]]);
		}

		var options = {
			title: '',
			vAxis: {minValue: 0},
			legend: { position: 'bottom' }
		};

		var options2 = {
			title: '',
			height: 400,
			width: '100%',
			vAxis: {minValue: 0},
			hAxis: { textPosition: 'none' },
			legend: { position: 'none' }
		};

		var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
		//var chart_open = new google.visualization.LineChart(document.getElementById('curve_chart_open'));

		chart.draw(data, options);
		//chart_open.draw(data, options2);
		//drawChart2();
		
	}

	//google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart2);

	function drawChart2() {

		var years2 = [<?php foreach ($tijd as $t) { echo "'".$t . "',"; } ?>];
		var impactsaldo2 = [<?php $m_s_tot = 0; foreach ($mutatie_saldo as $m_s) { $m_s_tot += $m_s; echo $m_s_tot . ","; } ?>];
		var impactpunten2 = [<?php $m_p_tot = 0; foreach ($mutatie_punten as $m_p) { $m_p_tot += $m_p; echo $m_p_tot . ","; } ?>];
		//var impactzegels2 = [<?php $m_z_tot = 0; foreach ($mutatie_zegels as $m_z) { $m_z_tot += $m_z; echo $m_z_tot . ","; } ?>];

		var data = new google.visualization.DataTable();
		data.addColumn('string', 'years');
		data.addColumn('number', 'impactsaldo');
		data.addColumn('number', 'impactpunten');
		//data.addColumn('number', 'impactzegels');

		for(i = 0; i < years2.length; i++) {
			data.addRow([years2[i], impactsaldo2[i], impactpunten2[i]]);
		}

		var options = {
			title: '',
			vAxis: {minValue: 0},
			legend: { position: 'bottom' }
		};

		var options2 = {
			title: '',
			height: 400,
			width: '100%',
			vAxis: {minValue: 0},
			hAxis: { direction: -1 },
			legend: { position: 'bottom' }
		};

		//var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
		var chart_open = new google.visualization.LineChart(document.getElementById('curve_chart_open'));

		//chart.draw(data, options);
		chart_open.draw(data, options);
	}
</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

<script>
	$(document).ready( function () {
		
		$('.datatable').DataTable( 
		{
			"language": {
				"sProcessing": "Bezig...",
				"sLengthMenu": "_MENU_ resultaten weergeven",
				"sZeroRecords": "Geen resultaten gevonden",
				"sInfo": "_START_ tot _END_ van _TOTAL_ resultaten",
				"sInfoEmpty": "Geen resultaten om weer te geven",
				"sInfoFiltered": " (gefilterd uit _MAX_ resultaten)",
				"sInfoPostFix": "",
				"sSearch": "Zoeken:",
				"sEmptyTable": "Geen resultaten aanwezig in de tabel",
				"sInfoThousands": ".",
				"sLoadingRecords": "Een moment geduld aub - bezig met laden...",
				"oPaginate": {
					"sFirst": "Eerste",
					"sLast": "Laatste",
					"sNext": "Volgende",
					"sPrevious": "Vorige"
				},
				"oAria": {
					"sSortAscending":  ": activeer om kolom oplopend te sorteren",
					"sSortDescending": ": activeer om kolom aflopend te sorteren"
				}
			}
		}
		);
	});
</script>

