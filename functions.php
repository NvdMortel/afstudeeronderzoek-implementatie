<?php


/*
*   Changelog
*   13-10-2018
*   Removed ACF Settings page
*   Better theme struction, moved includes to special folder
*/


//Include the Bootstrap 4 navwalker
require_once('bs4navwalker.php');




//Register default menu's
register_nav_menu( 'mainmenu', 'Mainmenu' );
register_nav_menu( 'footermenu', 'Footermenu' );
register_nav_menu( 'legalmenu', 'Legalmenu' );




//Enables asynchronized loading of scripts
function lemon_async_scripts($url)
{
    if ( strpos( $url, '#asyncload') === false )
        return $url;
    else if ( is_admin() )
        return str_replace( '#asyncload', '', $url );
    else
	return str_replace( '#asyncload', '', $url )."' async='async";
    }
add_filter( 'clean_url', 'lemon_async_scripts', 11, 1 );




//Enqueue the scripts for loading
function lemon_theme_scripts()
{
    // wp_enqueue_script() syntax, $handle, $src, $deps, $version, $in_footer(boolean)
    wp_enqueue_script('bootstrap-script', get_template_directory_uri().'/js/bootstrap.bundle.min.js#asyncload', array(), false, true);
    wp_enqueue_script('fancybox-script', get_template_directory_uri().'/js/jquery.fancybox.min.js#asyncload', array(), false, true);
    wp_enqueue_script('js-cookie', get_template_directory_uri().'/js/js.cookie.min.js#asyncload', array(), false, true);
    wp_enqueue_script('slick-script', get_template_directory_uri().'/js/slick.min.js#asyncload', array(), false, true);
    wp_enqueue_script('modernizr', get_template_directory_uri().'/js/modernizr.js#asyncload', array(), false, true);
    wp_enqueue_script('font-awesome', 'https://use.fontawesome.com/releases/v5.0.2/js/all.js#asyncload', array(), false, true);
	wp_enqueue_script('default', get_template_directory_uri().'/js/default.js#asyncload', array(), false, true);
}
add_action( 'wp_enqueue_scripts', 'lemon_theme_scripts');




//Enqueue scripts & styles the regular way
function enqueue_files() {

    // Styles
    wp_enqueue_style('bootstrap-style', get_template_directory_uri().'/css/bootstrap.min.css');
    wp_enqueue_style('fancybox-style', get_template_directory_uri().'/css/jquery.fancybox.min.css');
    wp_enqueue_style('slick-style', get_template_directory_uri().'/css/slick.min.css');
	wp_enqueue_style('style', get_stylesheet_uri(), false, filemtime(get_stylesheet_directory() . '/style.css'));
	wp_enqueue_style('style', get_stylesheet_uri(), false, filemtime(get_stylesheet_directory() . '/style.scss'));
	//wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Poppins:300,600,700|Roboto:300,900'); // Uncomment for Google Fonts

	// Scripts
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.3.1', true);

}
add_action( 'wp_enqueue_scripts', 'enqueue_files' );


//Declare custom taxonomies
/*add_action( 'init', 'create_topics_nonhierarchical_taxonomy', 0 );

function create_topics_nonhierarchical_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Taxonomies', 'Taxonomy General Name', '2lemon' ),
		'singular_name'              => _x( 'Taxonomy', 'Taxonomy Singular Name', '2lemon' ),
		'menu_name'                  => __( 'Taxonomy', '2lemon' ),
		'all_items'                  => __( 'All Items', '2lemon' ),
		'parent_item'                => __( 'Parent Item', '2lemon' ),
		'parent_item_colon'          => __( 'Parent Item:', '2lemon' ),
		'new_item_name'              => __( 'New Item Name', '2lemon' ),
		'add_new_item'               => __( 'Add New Item', '2lemon' ),
		'edit_item'                  => __( 'Edit Item', '2lemon' ),
		'update_item'                => __( 'Update Item', '2lemon' ),
		'view_item'                  => __( 'View Item', '2lemon' ),
		'separate_items_with_commas' => __( 'Separate items with commas', '2lemon' ),
		'add_or_remove_items'        => __( 'Add or remove items', '2lemon' ),
		'choose_from_most_used'      => __( 'Choose from the most used', '2lemon' ),
		'popular_items'              => __( 'Popular Items', '2lemon' ),
		'search_items'               => __( 'Search Items', '2lemon' ),
		'not_found'                  => __( 'Not Found', '2lemon' ),
		'no_terms'                   => __( 'No items', '2lemon' ),
		'items_list'                 => __( 'Items list', '2lemon' ),
		'items_list_navigation'      => __( 'Items list navigation', '2lemon' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
    register_taxonomy( 'taxonomy', array( 'post' ), $args );


}*/
//End taxonomies




//Declare Custom Post Types
/* function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Post Types', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Post Type', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Post Types', '2lemon' ),
		'name_admin_bar'        => __( 'Post Type', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Post Type', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => false,
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
    register_post_type( 'post_type', $args );


flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type', 0 );
*/

function custom_post_type_examens() {

	$labels = array(
		'name'                  => _x( 'Opgaven', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Opgave', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Opgaven', '2lemon' ),
		'name_admin_bar'        => __( 'opgave', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Opgave', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => 'opgave',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Opgave', $args );

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_examens', 0 );


function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Leerdoelen', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Leerdoel', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Leerdoelen', '2lemon' ),
		'name_admin_bar'        => __( 'Leerdoel', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Leerdoel', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '/leerdoel')
	);
    register_post_type( 'Leerdoel', $args );


flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type', 0 );



function custom_post_type_sector() {

	$labels = array(
		'name'                  => _x( 'Sector', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Sector', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Sector', '2lemon' ),
		'name_admin_bar'        => __( 'Sector', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Sector', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/sector',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Sector', $args );

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_sector', 0 );


function custom_post_type_inhoudslijn() {

	$labels = array(
		'name'                  => _x( 'Inhoudslijn', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Inhoudslijn', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Inhoudslijn', '2lemon' ),
		'name_admin_bar'        => __( 'inhoudslijn', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Inhoudslijn', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/inhoudslijn',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Inhoudslijn', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_inhoudslijn', 0 );

function custom_post_type_vakken() {

	$labels = array(
		'name'                  => _x( 'Vakken', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Vak', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Vakken', '2lemon' ),
		'name_admin_bar'        => __( 'vak', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Vak', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Vak', $args );
    //register_post_type( 'Vakken', $args );

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_vakken', 0 );


function custom_post_type_vakkern() {

	$labels = array(
		'name'                  => _x( 'Vakkernen', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Vakkern', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Vakkernen', '2lemon' ),
		'name_admin_bar'        => __( 'vakkern', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Vakkern', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/vakkern',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Vakkern', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_vakkern', 0 );


function custom_post_type_subkern() {

	$labels = array(
		'name'                  => _x( 'Subkernen', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Subkern', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Subkernen', '2lemon' ),
		'name_admin_bar'        => __( 'subkern', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Subkernen', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/subkern',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Subkern', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_subkern', 0 );

function custom_post_type_examenprogramma() {

	$labels = array(
		'name'                  => _x( 'Examenprogramma', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Examenprogramma', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Examenprogramma', '2lemon' ),
		'name_admin_bar'        => __( 'examenprogramma', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Examenprogramma', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Examenprogramma', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_examenprogramma', 0 );

function custom_post_type_syllabus() {

	$labels = array(
		'name'                  => _x( 'Syllabus', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Syllabus', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Syllabus', '2lemon' ),
		'name_admin_bar'        => __( 'syllabus', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Syllabus', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Syllabus', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_syllabus', 0 );

function custom_post_type_domein() {

	$labels = array(
		'name'                  => _x( 'Domeinen', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Domein', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Domeinen', '2lemon' ),
		'name_admin_bar'        => __( 'domein', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Domeinen', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/domein',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Domein', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_domein', 0 );


function custom_post_type_subdomein() {

	$labels = array(
		'name'                  => _x( 'Subdomeinen', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Subdomein', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Subdomeinen', '2lemon' ),
		'name_admin_bar'        => __( 'subdomein', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Subdomein', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/subdomein',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'Subdomein', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_subdomein', 0 );


function custom_post_type_onderwerp() {

	$labels = array(
		'name'                  => _x( 'Onderwerpen', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Onderwerp', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Onderwerpen', '2lemon' ),
		'name_admin_bar'        => __( 'onderwerp', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Onderwerp', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => '%category%/onderwerp',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'onderwerp', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_onderwerp', 0 );


function custom_post_type_project() {

	$labels = array(
		'name'                  => _x( 'Projecten', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Project', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Projecten', '2lemon' ),
		'name_admin_bar'        => __( 'project', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Project', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => 'project',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'project', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_project', 0 );


function custom_post_type_nota() {

	$labels = array(
		'name'                  => _x( 'Notas', 'Post Type General Name', '2lemon' ),
		'singular_name'         => _x( 'Nota', 'Post Type Singular Name', '2lemon' ),
		'menu_name'             => __( 'Notas', '2lemon' ),
		'name_admin_bar'        => __( 'nota', '2lemon' ),
		'archives'              => __( 'Item Archives', '2lemon' ),
		'attributes'            => __( 'Item Attributes', '2lemon' ),
		'parent_item_colon'     => __( 'Parent Item:', '2lemon' ),
		'all_items'             => __( 'All Items', '2lemon' ),
		'add_new_item'          => __( 'Add New Item', '2lemon' ),
		'add_new'               => __( 'Add New', '2lemon' ),
		'new_item'              => __( 'New Item', '2lemon' ),
		'edit_item'             => __( 'Edit Item', '2lemon' ),
		'update_item'           => __( 'Update Item', '2lemon' ),
		'view_item'             => __( 'View Item', '2lemon' ),
		'view_items'            => __( 'View Items', '2lemon' ),
		'search_items'          => __( 'Search Item', '2lemon' ),
		'not_found'             => __( 'Not found', '2lemon' ),
		'not_found_in_trash'    => __( 'Not found in Trash', '2lemon' ),
		'featured_image'        => __( 'Featured Image', '2lemon' ),
		'set_featured_image'    => __( 'Set featured image', '2lemon' ),
		'remove_featured_image' => __( 'Remove featured image', '2lemon' ),
		'use_featured_image'    => __( 'Use as featured image', '2lemon' ),
		'insert_into_item'      => __( 'Insert into item', '2lemon' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', '2lemon' ),
		'items_list'            => __( 'Items list', '2lemon' ),
		'items_list_navigation' => __( 'Items list navigation', '2lemon' ),
		'filter_items_list'     => __( 'Filter items list', '2lemon' ),
	);
	$args = array(
		'label'                 => __( 'Nota', '2lemon' ),
		'description'           => __( 'Post Type Description', '2lemon' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 			=> array('slug' => 'nota',
			'with_front' => false,
        'hierarchical' => true)
	);
    register_post_type( 'nota', $args );
 

flush_rewrite_rules();
}

//Register the CPT
add_action( 'init', 'custom_post_type_nota', 0 );

function wpa_course_post_link( $post_link, $id = 0 ){
    $post = get_post($id);
    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, 'category' );
        if( $terms ){
            return str_replace( '%category%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;
}
add_filter( 'post_type_link', 'wpa_course_post_link', 1, 3 );





/*Change default CPT titles
function change_cpt_placeholders($title) {
    $screen = get_current_screen();
    if(isset($screen->post_type)) {
        if($screen->post_type == "post") {
            $title = "Titel nieuwsbericht";
        }
        if($screen->post_type == "vacatures") {
            $title = "Vacaturetitel";
        }
        if($screen->post_type == "team") {
            $title = "Voor- en achternaam medewerker";
        }
        if($screen->post_type == "fotoalbums") {
            $title = "Albumtitel";
        }
        if($screen->post_type == "agenda") {
            $title = "Naam evenement (Let op: geen datum!)";
        }
    }
    return $title;
}
add_filter('enter_title_here','change_cpt_placeholders');
*/




//Register wiget area -> https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
/*
function lemon_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', '2lemon' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', '2lemon' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'lemon_widgets_init' );
*/


//Switch to valid html5 output for the stated forms
add_theme_support( 'html5', array(
	'search-form',
	'comment-form',
	'comment-list',
	'gallery',
	'caption',
) );



//Disable WP pingback functions
function disable_xmlrpc_pingback( $methods ) {
    unset( $methods['pingback.ping'] );
    return $methods;
};
add_filter( 'xmlrpc_methods', 'disable_xmlrpc_pingback' );


//Add theme title support -> https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
function theme_title_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_title_setup' );

//Generate ACF page
if(function_exists('acf_add_options_page')){
    acf_add_options_page(array(
        'page_title'    => 'Bedrijfsgegevens instellen',
        'menu_title'    => 'Bedrijfsgegevens',
        'menu_slug'     => 'bedrijfsgegevens',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

//Generate ACF settings page, specific for the theme
//Added 13-10-2018
if(function_exists('acf_add_options_page')){
    acf_add_options_page(array(
        'page_title'    => 'Site instellingen',
        'menu_title'    => 'Site instellingen',
        'menu_slug'     => 'site-instellingen',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}


function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

// function add_meta_keys_to_revision( $keys ) {
// 	$keys[] = 'user_content_havo');
// 	return $keys;
// }
// add_filter( 'wp_post_revision_meta_keys', 'add_meta_keys_to_revision' );

function my_pre_save_post( $post_id ) {
    // bail early if editing in admin
    // ACF already handles admin revisions correctly
    if( is_admin() ) {
      return $post_id;
    }
    // force a post update, this will generate a revision and
    // trigger the '_wp_put_post_revision' action
    wp_update_post( get_post($post_id) );
    // allow acf to update the parent post meta normally
    return $post_id;
}
add_filter('acf/pre_save_post' , 'my_pre_save_post' );

// revision just got created
// detect all of the acf fields on the original and
// apply them to the revision
function _on_wp_put_post_revision( $revision_id ) {
  // bail early if editing in admin
  if( is_admin() ) {
    return;
  }
  // get the revision post object
  $revision = get_post( $revision_id );
  // get the id of the original post
  $post_id  = $revision->post_parent;
  // A lot of this is copied from ACF's revision class
  // get all the post meta from the original post
  $custom_fields = get_post_custom( $post_id );

  if( !empty($custom_fields) ) {
    foreach( $custom_fields as $k => $v ) {
      // value is always an array
      $v = $v[0];
      // bail early if $value is not is a field_key
      if( !acf_is_field_key($v) ) {
        continue;
      }
      // remove prefix '_' field from reference
      $field_name = substr($k, 1);
      // update the field value using the POST value supplied in the form
      update_field($field_name, $_POST['acf'][$v], $revision_id);
      // add the reference key
      update_metadata('post', $revision_id, $k, $v);
    }
  }
}
add_action( '_wp_put_post_revision', '_on_wp_put_post_revision' );


function comment_reform ($arg) {
$arg['title_reply'] = __('Start een discussie');
return $arg;
}
add_filter('comment_form_defaults','comment_reform');


// //Modify TinyMCE editor to hide H1.
// function tiny_mce_remove_unused_formats( $initFormats ) {
//     // Add block format elements you want to show in dropdown
//     $initFormats['block_formats'] = 'Preformatted=pre';
//      $style_formats = array(
//         array(
//             'title' => 'Paragraph',
//             'styles' => array(
//                 'margin' => '0px'
//             )
//         )
//     );

//     $initFormats['style_formats'] = json_encode( $style_formats );
//     return $initFormats;
// }
// add_filter( 'tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );


add_action( 'after_setup_theme', 'mythemeslug_theme_setup' );

if ( ! function_exists( 'mythemeslug_theme_setup' ) ) {
	function mythemeslug_theme_setup(){
		/********* Registers an editor stylesheet for the theme ***********/
		add_action( 'admin_init', 'mythemeslug_theme_add_editor_styles' );
		/********* TinyMCE Buttons ***********/
		add_action( 'init', 'mythemeslug_buttons' );
	}
}

/********* Registers an editor stylesheet for the theme ***********/
if ( ! function_exists( 'mythemeslug_theme_add_editor_styles' ) ) {
	function mythemeslug_theme_add_editor_styles() {
	    add_editor_style( 'custom-editor-style.css' );
	}
}

/********* TinyMCE Buttons ***********/
if ( ! function_exists( 'mythemeslug_buttons' ) ) {
	function mythemeslug_buttons() {
		if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
	        return;
	    }

	    if ( get_user_option( 'rich_editing' ) !== 'true' ) {
	        return;
	    }

	    add_filter( 'mce_external_plugins', 'mythemeslug_add_buttons' );
	    add_filter( 'mce_buttons', 'mythemeslug_register_buttons' );
	}
}

if ( ! function_exists( 'mythemeslug_add_buttons' ) ) {
	function mythemeslug_add_buttons( $plugin_array ) {
	    $plugin_array['dashlist'] = get_template_directory_uri().'/js/tinymce_buttons.js';
	    $plugin_array['numberlist'] = get_template_directory_uri().'/js/tinymce_buttons_2.js';
	    $plugin_array['letterlist'] = get_template_directory_uri().'/js/tinymce_buttons_letterlist.js';
	    $plugin_array['letterlist-small'] = get_template_directory_uri().'/js/tinymce_buttons_letterlist_small.js';
	    $plugin_array['note'] = get_template_directory_uri().'/js/tinymce_buttons_note.js';
	    $plugin_array['border'] = get_template_directory_uri().'/js/tinymce_buttons_border.js';
	    $plugin_array['all-border'] = get_template_directory_uri().'/js/tinymce_buttons_all_border.js';
	    $plugin_array['border-full-width'] = get_template_directory_uri().'/js/tinymce_buttons_border_full_width.js';
	    $plugin_array['border-multiple-choice'] = get_template_directory_uri().'/js/tinymce_buttons_border_multiple_choice.js';
	    $plugin_array['formula-box'] = get_template_directory_uri().'/js/tinymce_buttons_formula_box.js';
	    $plugin_array['fracture'] = get_template_directory_uri().'/js/tinymce_buttons_fracture.js';
	    $plugin_array['fracture-small'] = get_template_directory_uri().'/js/tinymce_buttons_fracture_small.js';
	    $plugin_array['square'] = get_template_directory_uri().'/js/tinymce_buttons_square.js';
	    //$plugin_array['square-fracture'] = get_template_directory_uri().'/js/tinymce_buttons_square_fracture.js';
	    $plugin_array['supsub'] = get_template_directory_uri().'/js/tinymce_buttons_supsub.js';
	    $plugin_array['supsub-fracture'] = get_template_directory_uri().'/js/tinymce_buttons_supsub_fracture.js';
	    $plugin_array['big-hooks'] = get_template_directory_uri().'/js/tinymce_buttons_big_hooks.js';
	    $plugin_array['text-alinea'] = get_template_directory_uri().'/js/tinymce_buttons_text_alinea.js';
	    $plugin_array['text-alinea-with-nr'] = get_template_directory_uri().'/js/tinymce_buttons_text_alinea_with_nr.js';
	    $plugin_array['gap-text'] = get_template_directory_uri().'/js/tinymce_buttons_gap_text.js';
	    // $plugin_array['div'] = get_template_directory_uri().'/js/tinymce_buttons_div.js';
	    $plugin_array['column-2'] = get_template_directory_uri().'/js/tinymce_buttons_column_2.js';
	    return $plugin_array;
	}
}

if ( ! function_exists( 'mythemeslug_register_buttons' ) ) {
	function mythemeslug_register_buttons( $buttons ) {
	    array_push( $buttons, 'dashlist' );
	    array_push( $buttons, 'numberlist' );
	    array_push( $buttons, 'letterlist' );
	    array_push( $buttons, 'letterlist-small' );
	     array_push( $buttons, 'note' );
	    array_push( $buttons, 'border' );
	    array_push( $buttons, 'border-full-width' );
	    array_push( $buttons, 'all-border' );
	    array_push( $buttons, 'border-multiple-choice' );
	    array_push( $buttons, 'formula-box' );
	    array_push( $buttons, 'fracture' );
	    array_push( $buttons, 'fracture-small' );
	     array_push( $buttons, 'square' );
	     // array_push( $buttons, 'square-fracture' );
	      array_push( $buttons, 'supsub' );
	      array_push( $buttons, 'supsub-fracture' );
	      array_push( $buttons, 'big-hooks' );
	      array_push( $buttons, 'text-alinea' );
	      array_push( $buttons, 'text-alinea-with-nr' );
	      array_push( $buttons, 'gap-text' );
	      array_push( $buttons, 'column-2' );
	      // array_push( $buttons, 'div' );
	    return $buttons;
	}
}




function change_mce_options($init){
    //$init["forced_root_block"] = '';
    //$init["font_formats"] = 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n';
    //$init["style_formats"] = array( array( 'title' => 'Paragraph', 'styles' => array( 'color' => '#ff0000')));
$init['indentation'] = '10px';
    $init['block_formats'] = 'Paragraph=p;Preformatted=pre';
    $init["style_formats"] = json_encode(array( array( 'title' => 'Paragraph', 'class' => 'text', 'styles' => array( 'color' => 'red'))));
    $init['fontsize_formats'] = "11px 12px 14px 16px 18px 22px 24px 28px 32px 36px 40px 44px 48px 52px";// Add as needed
    //$init["forced_root_block"] = false;
    // $init["force_br_newlines"] = true;
    // $init["force_p_newlines"] = false;
    // $init["convert_newlines_to_brs"] = true;
    //$init['selector'] = 'textarea';
    $init['content_style'] = '* { font-family: Arial; font-size: 12pt; }   p {margin: 0} table td { min-width:20px; } blockquote table { display:inline-table;} blockquote p { display: inline-block; } .dashlist { border: none; } .dashlist td, .numberlist td, .letterlist td { vertical-align: top;  } .border { border: 2px solid #e9e9e9; padding: 20px; padding-top: 10px; padding-bottom: 10px; } .all-border { border: none; border-spacing: 0px; } .all-border table { border: none; } .all-border table td { border: 2px solid #e9e9e9; } .border { display: inline-block; } .formula-box { font-family: Times New Roman; min-width:100px; min-height:20px; border:1px solid #e9e9e9; } ..formula-box table, .formula-box p { display: inline-block; vertical-align: middle; text-align:center; } .formula-box td, .formula-box td p { font-family: Times New Roman; font-size:18px; } .fracture-bottom { border-top: 1px solid black; } .border-full-width { margin-top:15px; margin-bottom:15px;} .fracture-small-bottom, .fracture-small-top { font-size: 12px!important; } sub, sup { font-size:12px; } .text-alinea { border: 1px dashed black; } .gap-number { border-bottom: 1px solid black; font-weight: bold; padding-left:10px; padding-right:10px; } .text-alinea-with-nr { margin-left:30px; } .text-alinea-with-nr  .alinea-nr { margin-left:-30px; }.text-alinea-with-nr  .text-alinea { margin-top: -21px;  } ';
    //$init['plugins'] = 'advlist';
   // $init['advlist_bullet_styles'] = "{ title: 'test' }";
    //$init['advlist_bullet_styles']['styles']['listStyleType'] = 'square';
    return $init;
}
add_filter('tiny_mce_before_init','change_mce_options');


remove_filter ('acf_the_content', 'wpautop');


function my_awesome_func( $request_data ) {
	$parameters = $request_data->get_params();
	$posts = get_posts( array(
			'post_type' => array( 'opgave' ), // This is the line that allows to fetch multiple post types.
			'meta_query' => array(
				array(
					'key' => 'examen_niveau',
					'value' => '"' . $parameters['niveau'] . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'examen_jaar',
					'value' => '"' . $parameters['jaar'] . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'examen_tijdvak',
					'value' => '"' . $parameters['tijdvak'] . '"',
					'compare' => 'LIKE'
				),
				array(
					'key' => 'examen_vragen_vraagnummer',
					'value' => "'" . $parameters['vraagnummer'] . "'",
					'compare' => 'LIKE'
				),
				array(
					'key' => 'opgavenummer',
					'value' => $parameters['opgavenummer'],
					'compare' => 'LIKE'
				),


			)
		)
	);

	if ( empty( $posts ) ) {
	  return 'no post found!';
	}
   $titles = array();
   foreach($posts as $post){
	   array_push($titles,$post->post_title);
   }
	return $titles;
  }

  add_action( 'rest_api_init', function () {
	register_rest_route( 'opgaven/v1', '/vragen', array(
	  'methods' => 'GET',
	  'callback' => 'my_awesome_func',
	) );
  } );

  function set_query_vars( $query_vars ) {

    $query_vars[] = 'vak';
    $query_vars[] = 'niveau';
    $query_vars[] = 'jaren';
    $query_vars[] = 'examenduur';
    $query_vars[] = 'moeilijkheidsgraad';
    $query_vars[] = 'soortvragen';
    return $query_vars;

  }

  add_filter( 'query_vars', 'set_query_vars' );


