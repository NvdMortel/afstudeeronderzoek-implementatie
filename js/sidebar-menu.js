
$( document ).ready(function() {

	$('#dashboard').show();
	$('#informatie').hide();
	$('#impactlijst').hide();
	$('#profiel').hide();
	$('#administratie').hide();
	$('#accountgegevens').hide();
	$('#dashboard-link').addClass('active');
	$('#informatie-link').removeClass('active');
	$('#impactlijst-link').removeClass('active');
	$('#profiel-link').removeClass('active');
	$('#administratie-link').removeClass('active');
	$('#accountgegevens-link').removeClass('active');
	
	$('#dashboard-link').click(function(event) {
		event.preventDefault(); //fix reload to homepage
		$('#dashboard').show();
		$('#informatie').hide();
		$('#impactlijst').hide();
		$('#profiel').hide();
		$('#administratie').hide();
		$('#accountgegevens').hide();
		$('#dashboard-link').addClass('active');
		$('#informatie-link').removeClass('active');
		$('#impactlijst-link').removeClass('active');
		$('#profiel-link').removeClass('active');
		$('#administratie-link').removeClass('active');
		$('#accountgegevens-link').removeClass('active');
	});

	$('#informatie-link').click(function(event) {
		event.preventDefault();
		$('#dashboard').hide();
		$('#informatie').show();
		$('#impactlijst').hide();
		$('#profiel').hide();
		$('#administratie').hide();
		$('#accountgegevens').hide();
		$('#dashboard-link').removeClass('active');
		$('#informatie-link').addClass('active');
		$('#impactlijst-link').removeClass('active');
		$('#profiel-link').removeClass('active');
		$('#administratie-link').removeClass('active');
		$('#accountgegevens-link').removeClass('active');
	});

	$('#impactlijst-link').click(function(event) {
		event.preventDefault();
		$('#dashboard').hide();
		$('#informatie').hide();
		$('#impactlijst').show();
		$('#profiel').hide();
		$('#administratie').hide();
		$('#accountgegevens').hide();
		$('#dashboard-link').removeClass('active');
		$('#informatie-link').removeClass('active');
		$('#impactlijst-link').addClass('active');
		$('#profiel-link').removeClass('active');
		$('#administratie-link').removeClass('active');
		$('#accountgegevens-link').removeClass('active');
	});

	$('#profiel-link').click(function(event) {
		event.preventDefault();
		$('#dashboard').hide();
		$('#informatie').hide();
		$('#impactlijst').hide();
		$('#profiel').show();
		$('#administratie').hide();
		$('#accountgegevens').hide();
		$('#dashboard-link').removeClass('active');
		$('#informatie-link').removeClass('active');
		$('#impactlijst-link').removeClass('active');
		$('#profiel-link').addClass('active');
		$('#administratie-link').removeClass('active');
		$('#accountgegevens-link').removeClass('active');
	});

	$('#administratie-link').click(function(event) {
		event.preventDefault();
		$('#dashboard').hide();
		$('#informatie').hide();
		$('#impactlijst').hide();
		$('#profiel').hide();
		$('#administratie').show();
		$('#accountgegevens').hide();
		$('#dashboard-link').removeClass('active');
		$('#informatie-link').removeClass('active');
		$('#impactlijst-link').removeClass('active');
		$('#profiel-link').removeClass('active');
		$('#administratie-link').addClass('active');
		$('#accountgegevens-link').removeClass('active');
	});

	$('#accountgegevens-link').click(function(event) {
		event.preventDefault();
		$('#dashboard').hide();
		$('#informatie').hide();
		$('#impactlijst').hide();
		$('#profiel').hide();
		$('#administratie').hide();
		$('#accountgegevens').show();
		$('#dashboard-link').removeClass('active');
		$('#informatie-link').removeClass('active');
		$('#impactlijst-link').removeClass('active');
		$('#profiel-link').removeClass('active');
		$('#administratie-link').removeClass('active');
		$('#accountgegevens-link').addClass('active');
	});

});
