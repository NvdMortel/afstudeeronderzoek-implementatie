<?php acf_form_head(); ?>
<?php get_header(); 


if (get_the_author_meta( 'ID' ) == get_current_user_id()) { ?>


	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/notas.css">

	<main class="wrapper project">

		<div class="container">

			<div class="row">
				<div class="col-12">

					<div id="primary">
						<div id="content" role="main">

							<b>Nota</b>

							<?php /* The loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<h1><?php the_title(); ?></h1>

							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-4">

						<p class="info">Zodra deze nota is uitbetaald moet je deze opgeven bij de Belastingdienst.</p>

						<table class="table">
							<thead>
								<tr>
									<th scope="col">DATUM</th>
									<th scope="col">BEDRAG</th>
									<th scope="col">STATUS</th>

								</tr>
							</thead>
							<tbody>

								<tr>
									<td><? echo get_the_date( 'd-m-Y' ); ?></td>
									<!-- <td>in uitvoering</td> -->
									<td>€ <? echo get_field('bedrag'); ?></td>
									<td><? if (get_field('status')) { echo get_field('status'); } else { echo "in behandeling"; } ?></td> 
								</tr>

							</tbody>
						</table>

					</div>
					<div class="col-1"></div>
					<div class="col-3 materiaal">

						<b>ADMINISTRATIE</b>
						<br>
						<a href="" target="_blank"  data-toggle="modal" data-target="#modal-gegevenswijzigen">Uitbetaalgegevens wijzigen ></a>

						<br>

						<? echo do_shortcode('[print-me target="div#print" title="Nota uitprinten >"]'); ?>

						<!-- <a href="javascript:window.print();">Print me</a> -->
						<!--  -->

					</div>
					<div class="col-4 materiaal">

						<b>UITBETAALD</b>
						<br>
						<p><? if (get_field('uitbetaald')) { echo get_field('uitbetaald'); } else { echo "Nog niet uitbetaald"; } ?></p>
						<? if (get_field('bankrekening', 'user_'.get_the_author_meta( 'ID' ))) { } else { echo '<i>Er is nog geen rekeningnummer bekend. <a href="" target="_blank"  data-toggle="modal" data-target="#modal-gegevenswijzigen">Invullen > </a></i>'; } ?>

					</div>

				</div>
				<hr>

				<div id="nota" class="nota">
					<div class="row" >		
						<div class="col-12" >

							<div class="row">	

								<div class="col-7 content" id="print" style="max-width:800px;" >

									<div>

										<div id="view"> 

											<img src="<?=get_template_directory_uri()?>/images/logo-exploronderwijs.png" alt="logo" style="width:160px;"/>

											<br><br><br>

											<h1><? if (get_field('bedrijfsnaam', 'user_'.get_the_author_meta( 'ID' ))) { echo 'Factuur'; } else { echo 'Kwitantie'; } ?></h1>
											<b id="ref">Referentie: <? if (get_field('notanummer')) { echo get_field('notanummer'); } else { echo 'Nog niet toegewezen'; } ?> </b>

											<p><strong>Ontvanger: </strong> <? echo get_field('voornaam', 'user_'.get_the_author_meta( 'ID' )) . " " . get_field('achternaam', 'user_'.get_the_author_meta( 'ID' )); ?>
											<br>
											<strong>Afnemer: </strong> Explor Onderwijs
											<br>
											<strong>Omschrijving: </strong><? if (get_field('omschrijving')) { echo get_field('omschrijving'); } else { echo '<i>Er is nog geen omschrijving van de werkzaamheden bekend.</i>'; } ?> 
											<br>
											<strong>Bedrag: </strong>€<? echo get_field('bedrag'); ?>

											<br><br>

											<div style="border: 1px dashed #e9e9e9; padding:20px;">

												<? if (get_field('ondertekeningsplaats')) { 
													?><br>
													<strong>Plaats: </strong><? if (get_field('ondertekeningsplaats')) { echo get_field('ondertekeningsplaats'); } else { echo 'Deze nota moet nog ondertekend worden.'; } ?>
													<br>
													<strong>Datum: </strong><? if (get_field('ondertekeningsdatum')) { echo get_field('ondertekeningsdatum'); } else { echo 'Deze nota moet nog ondertekend worden.'; } ?>
													<br>

													<br>
													<br>
													<strong>Handtekening </strong>

													<strong>Handtekening Explor Onderwijs</strong><?

												} else { 
													echo '<i>Deze nota moet nog ondertekend worden.</i>';
												}
												?>

											</div>

											<br>

											<i>Opmerkingen</i> 
											<br>Uitgereikt door afnemer

										</div> 
									</div>

								</div>

								<div class="col-5">

									<br><br>
									<h2>Items</h2>
									<p>De volgende items worden uitbetaald. Let op dat je hierover ook geen royalties meer zult ontvangen.</p>
									<br>

									<? 
									if( have_rows('transacties', 'user_'.get_the_author_meta( 'ID' ))):

										while( have_rows('transacties', 'user_'.get_the_author_meta( 'ID' )) ) : the_row();

											
											if (get_sub_field('handeling') == 'royalties') {

												$counter++;

												if ($counter == 1) {
													?><table class="table">
														<thead>
															<tr>
																<th scope="col">#</th>
																<th scope="col">Datum</th>
																<th scope="col">ID</th>

															</tr>
														</thead>
														<tbody><?
													}

													echo "<tr>";
													echo "<td>".$counter."</td>";

													echo "<td>".get_sub_field('tijdstempel')."</td>";
													echo "<td>Royalty ". get_the_title(get_sub_field('project')->ID)."</td>";
													echo "</td></tr>";
												}

											endwhile;

										endif; 

										if ($counter > 0) {
											echo '</tbody></table>';
										}
										else {
											?><i style="font-size:12px;">Een specificatie hiervan volgt later</i><?
										}

										?>


									</div>
								</div>

							</div>

						<?php endwhile; ?>

					</div>
				</div>

			</div>

		</main>

		<!-- Modal --> 
		<div class="modal fade" id="modal-gegevenswijzigen" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Uitbetaalgegevens wijzigen</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Wijzig hier je gegevens indien nodig. Dit kan alleen als de status van de nota nog in behandeling is. </p>

						<br>
						<p>
							<?
							$options = array(
								'fields' => array('voornaam', 'achternaam', 'bedrijfsnaam', 'bankrekening'),
								'post_id' => 'user_'.get_current_user_id()
							);
							acf_form($options); 

					//echo do_shortcode('[advanced_form form="form_5dea376458ec1"]');
							?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
						</div>
					</div>
				</div>
			</div>


			<?php get_footer(); 

		}
		else {
			wp_redirect('https://dev.onderwijscurriculum.nl');
		}
		?>
