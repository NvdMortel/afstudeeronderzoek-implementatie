<?php acf_form_head(); ?>
<?php get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/projecten.css">

<main class="wrapper project">
	
	<div class="container">
		
		<div class="row">
			<div class="col-12">

				<div id="primary">
					<div id="content" role="main">

						<? if (get_field('vergoeding_saldo')) {
							echo '<b>Freelance project</b>';
						}
						else if (get_field('vergoeding_punten')) {
							echo '<b>Community project</b>';
						}
						else {
							echo '<b>Projectvoorstel</b>';
						}
						?>

						<?php /* The loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<h1><?php the_title(); ?></h1>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-4">

					<p class="info">Op deze pagina vind je informatie rondom het project <?php the_title(); ?></p>

					<table class="table">
						<thead>
							<tr>
								<th scope="col">VERGOEDING</th>
								<th scope="col">BUDGET</th>
								<th scope="col">STATUS</th>

							</tr>
						</thead>
						<tbody>

							<tr>
								<td><? if (get_field('vergoeding_saldo')) {
									echo '<img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactsaldo.png"> ';
									echo get_field('vergoeding_saldo'); 
								}
								else if (get_field('vergoeding_punten')) {
									echo '<img src="https://dev.onderwijscurriculum.nl/wp-content/themes/themabasis-v2/images/impactpunt.png"> ';
									echo get_field('vergoeding_punten'); 
								}
								else {
									echo 'niet bekend';
								} ?>

							</td>
							<!-- <td>in uitvoering</td> -->
							<td><? if (get_field('budget')) { echo '€' . get_field('budget'); } else { echo 'niet bekend'; } ?></td>
							<td><? echo get_field('status'); ?></td> <!-- //besloten//op uitnodiging//gesloten -->

						</tr>

					</tbody>
				</table>

			</div>
			<div class="col-1"></div>
			<div class="col-3 materiaal">

				<b>ADMINISTRATIE</b>
				<br>

				<a data-toggle="modal" data-target="#modal-inschrijveninfo">Schrijf je in voor een taak ></a>
				<!-- <a href="" target="_blank"  data-toggle="modal" data-target="#ModalLoginForm">Schrijf je in voor een taak ></a> -->

				<br>
				<a data-toggle="modal" data-target="#modal-declarereninfo">Opleveren en declareren ></a>

				<!-- 	<br><br>
					<b>GOEDKEURING</b>  -->
					<br>
					<a data-toggle="modal" data-target="#modal-stemminginfo">Stemming goedkeuring project ></a>

					<!-- <br>
						<a href="" target="_blank">Beoordeel uitgevoerde taken ></a>  -->

					</div>
					<div class="col-4 materiaal">

						<b>PROJECTLEIDER(S)</b>
						<br>
						<!-- <div class="circle">NM</div> -->
						<!-- <div class="circle" style="background-color:#007bff;">JS</div> -->

						<?php coauthors(); ?> 

						<br><br>

						<b>IMPACTMAKERS (UITVOERDERS)</b>
						<br>
						
						<?
						$users = array();

						if (have_rows('taken')):
							while (have_rows('taken')): the_row(); 
								$nickname = get_sub_field('uitvoerder')['nickname'];
								array_push($users, $nickname);
							endwhile;

							$users = array_unique($users); //array cleanup met alleen de unieke gebruikers
							foreach ($users as $user) {
								echo $user;
								echo ', ';
							}

						endif;
						?>

					</div>

				</div>
				<hr>

				<div class="projectinfo">
					<div class="row" >		
						<div class="col-12" >

							<div class="row">	

								<div class="col-7" style="z-index:1;">  
									<h2 class="justify-content-left">Projectbeschrijving</h2>

									<? 
									$coauthors = get_coauthors();  
									foreach ($coauthors as $coauthor) {
										//echo 
										if ($coauthor->ID == get_current_user_id()) {
											?><a id="edit-link" style="color:blue;">Bewerk project ></a><?
										}
									} ?>
									
								</div>

								<div class="col-7 content" >

									<div style="border-top:5px solid #e9e9e9;" >

										<div id="edit" >
											<br>
											<?php
											//the_field('user_content_havo');

											$options = array(
												'fields' => array('instructie'),
											);
											acf_form($options); 

											?>

										</div>

										<div id="view">

											
											<?echo the_content();?>
											<br>
											<b>Instructies</b>
											<?
											if(get_field('instructie')){
												the_field('instructie');
											}
											else {
												?><i>Geen instructies beschikbaar</i><?
											}
											?>
										</div>
									</div>

								</div>

								<div class="col-5 taken">

									<ul class="nav nav-pills justify-content-center tabs">
										<li class="nav-item">
											<a class="nav-link" href="#alle-taken">ALLE TAKEN</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#mijn-taken">MIJN TAKEN</a>
										</li>

										
									</ul>

									<div id='alle-taken'>

										
										<?
										$counter = 0;
										if (have_rows('taken')):

											while (have_rows('taken')): the_row(); 

												$counter++;

												if ($counter==1) {
													?><table class="table datatable">
														<thead>
															<tr>
																<th>ID</th>
																<th>BESCHRIJVING</th>
																<th>STATUS</th>
																<th>BRONNEN</th>
																<th>OPLEVERING</th>								
															</tr>
														</thead>
														<tbody><?
													}

													echo '<tr class="box copy-block-item">';
													echo '<td>'.$counter.'</td>';
													echo '<td>'.get_sub_field('beschrijving').'</td>';
													$value = get_sub_field('status');
													if ($value == "beschikbaar") {
														echo '<td><a id="'.$counter.'" data-toggle="modal" data-target="#modal-taakinschrijving">Beschikbaar ></a></td>';
													}
													else {
														echo '<td>'.$value.'</td>';
													}
													echo '<td><a id="'.$counter.'" data-toggle="modal" data-target="#modal-bronnen-'.$counter.'">Bekijk ></a></td>';
													
													echo '<td><a id="'.$counter.'" data-toggle="modal" data-target="#modal-resultaten">Bekijk ></a></td>';
													echo '</tr>';

													?><!-- Modal --> 
													<div class="modal fade" id="modal-bronnen-<? echo $counter; ?>" tabindex="-1" role="dialog" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">Bronnen</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<p>De volgende bronnen zijn nodig voor de uitvoering van deze taak:</p>
																	<br>
																	<?
																	if (have_rows('bronnen')) {

																		echo '<ul>';

																		while (have_rows('bronnen')): the_row(); 
																			echo '<li><a href="'.get_sub_field('link').'">'.get_sub_field('link').'</a></li>';
																			//echo get_field('link');
																			echo '<br>';

																		endwhile;

																		echo '</ul>';
																	}
																	else {
																		echo '<i>Er zijn geen bronnen bij deze taak.</i>';
																	}

																	?>

																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
																</div>
															</div>
														</div>
													</div>

													<!-- Modal --> 
													<div class="modal fade" id="modal-resultaten" tabindex="-1" role="dialog" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLabel">Resultaten</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<p>De volgende outputs zijn geleverd bij deze taak:</p>
																	<br>
																	<?
																	if (have_rows('outputs')) {

																		echo '<ul>';

																		while (have_rows('outputs')): the_row(); 
																			echo '<li><a href="'.get_sub_field('link').'">'.get_sub_field('link').'</a></li>';
																			//echo get_field('link');
																			echo '<br>';

																		endwhile;

																		echo '</ul>';
																	}
																	else {
																		echo '<i>Er zijn geen outputs bij deze taak.</i>';
																	}

																	?>

																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
																</div>
															</div>
														</div>
													</div>
													<?
												endwhile;
											endif;

											?>

										</tbody>
									</table>

								</div>



								<div id='mijn-taken'>


									<?

									if (have_rows('taken')):

										$counter = 0;

										while (have_rows('taken')): the_row(); 

											$counter++;

											if ($counter==1) {
												?><table class="table datatable">
													<thead>
														<tr>
															<th>ID</th>
															<th>BESCHRIJVING</th>
															<th>STATUS</th>
															<th>BRONNEN</th>
															<th>OPLEVERING</th>								
														</tr>
													</thead>
													<tbody>
													<? }

													if (get_sub_field('uitvoerder')['ID'] == get_current_user_id()) {

														echo '<tr class="box copy-block-item">';
														echo '<td>'.$counter.'</td>';
														echo '<td>'.get_sub_field('beschrijving').'</td>';
														$value = get_sub_field('status');
															//if ($value == "beschikbaar") {
														echo '<td><a id="'.$counter.'" data-toggle="modal" data-target="#modal-nieuwepost">Ga verder ></a></td>';
															//}
															//else {
															//	echo '<td>'.$value.'</td>';
															//}
														echo '<td><a id="'.$counter.'" data-toggle="modal" data-target="#modal-bronnen">Bekijk ></a></td>';
														echo '<td><a id="'.$counter.'" data-toggle="modal" data-target="#modal-resultaten">Bekijk ></a></td>';
														echo '</tr>';
													}
												endwhile;

												if ($counter == 0) {
													echo '<hr><i>Je hebt voor dit project nog geen taken opgepakt. <a>Schrijf je in ></a></i>';
												}
												else {
													?></tbody>
												</table>
												<?
											}
										endif;

										?>


									</div>

								</div>
							</div>

						</div>

					<?php endwhile; ?>

				</div>
			</div>


			<div class="row bijdragen">

				<div class="col-7 discussie">

					<b>Vragen of suggesties?</b>
					<?
					if ( comments_open() || get_comments_number() ) : 
						comments_template();
				endif; ?>

			</div>

			<div class="col-5 voorstellen">

				<br><br>

				<h2>Zelf een project beginnen?</h2> 
				<p>We hebben een impactfonds dat financiering van projecten mogelijk maakt. Dus heb jij een goed idee hoe je waarde kan toevoegen? Schrijf dan een projectvoorstel en dien deze in. De community gaat daarna stemmen over het voorstel waarbij na toekenning de financiering vrij komt. Ook kun je voorstellen doen om dit project te verbeteren.

					<br><br>
					<a data-toggle="modal" data-target="#modal-voorstellen" style="color:blue;">Projectvoorstel schrijven ></a></p>

				</div>
			</div>
		</div>

	</main>

	<!-- Modal --> 
	<div class="modal fade" id="modal-voorstellen" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Creëer voorstellen</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Heb je zelf een goed idee? Schrijf er een projectvoorstel van en dien het in. Bij goedkeuring krijg je financiering voor de uitvoering hiervan. Let op dat het schrijven 10 impactpunten kost en om het voorstel in te kunnen dienen kost het je 20 impactpunten.</p>

					<form method="post"> 
						<input name="titel" placeholder="Vul een projectitel in" required type="text" />
						<input type="submit" name="create" class="btn" value="Maak project aan"/> 
					</form> 

					<?php 

					if(isset($_POST['create'])) { 
						$post_data = array(
							'post_title' => $_POST["titel"],
        					'post_status' => 'publish', // Automatically publish the post.
        					'post_author' => get_current_user_id(),
        					'post_type' => 'project' 
        				);
						$link = wp_insert_post( $post_data);
					//TODO punten vermindering
					//update_field('bedrag', $_POST["bedrag"], $link);
						wp_redirect(get_permalink($link));
					} 

					?>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal --> 
	<div class="modal fade" id="modal-taakinschrijving" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Taak inschrijving</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Maak voor elke opgave van het examen een nieuwe post aan. Zodra je de eerste post aangemaakt hebt ben je ingeschreven voor deze taak en wordt er van je verwacht deze binnen de gestelde periode af te maken.</p>
					<br>
					<form method="post"> 
						<input name="titel" placeholder="Vul opdrachtnaam in" required type="text" />
						<input type="submit" name="task" class="btn" value="Start taakuitvoering"/> 
					</form> 

					<?php 

					if(isset($_POST['task'])) { 
						$post_data = array(
							'post_title' => $_POST["titel"],
        					'post_status' => 'publish', // Automatically publish the post.
        					'post_author' => get_current_user_id(),
        					'post_type' => 'opgave' 
        				);
						$link = wp_insert_post( $post_data);
							//TODO update row with author
							//$row = array(
							//	'uitvoerder'	=> get_current_user();
							//);

							//update_row('taken', $i, $row));

						wp_redirect(get_permalink($link));
					} 

					?>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal --> 
	<div class="modal fade" id="modal-nieuwepost" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Nieuwe opgave</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<p>Ga verder met deze taak</p>
					<i>Momenteel alleen nog mogelijk via de achterkant van Wordpress.</i>
<!-- 						--LIST--
-->					
<!-- <div class="box paste" id="paste-block-status"></div> -->

						<!-- <script>
							var test = $('#paste-block-status').innerHTML();
						</script>

						<? //echo '<script>document.writeln(test);</script>'; ?> -->


						<br><br>
						<p>Aangekomen bij een nieuwe opgave? Maak dan een nieuwe post aan. </p>

						<form method="post"> 
							<input name="titel" placeholder="Vul opdrachtnaam in" required type="text" />
							<input type="submit" class="btn" name="create" value="Creëer post"/> 
						</form> 

						<?php 

						if(isset($_POST['create'])) { 
							$post_data = array(
								'post_title' => $_POST["titel"],
        					'post_status' => 'publish', // Automatically publish the post.
        					'post_author' => get_current_user_id(),
        					'post_type' => 'opgave' 
        				);
							$link = wp_insert_post( $post_data);
					//TODO punten vermindering
					//update_field('bedrag', $_POST["bedrag"], $link);
							wp_redirect(get_permalink($link));
						} 

						?>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal --> 
		<div class="modal fade" id="modal-inschrijveninfo" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Schrijf je in voor een taak</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Om je in te schrijven voor een taak kijk of er nog taken beschikbaar zijn. Zo ja, klik op de link en volg de instructies.</p>

						<!-- De inschrijving voor een taak gaat op dit moment nog handmatig. Geef aan welke taken je op zou willen pakken en we koppelen je naam hieraan.</p> -->
						<br>
						<?php //echo do_shortcode( '[contact-form-7 id="7868" title="Inschrijven voor project"]' ); ?>
						<!-- <p>Zoek een taak uit waarvan de status nog op beschikbaar staat. Klik hier op en volg de instructies. Je schrijft je dan automatisch in voor deze taak. </p>

						<br>
						<p>Hulp nodig of vragen? Stuur ons dan een bericht of start een discussie</p> -->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal --> 
		<div class="modal fade" id="modal-declarereninfo" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Uren declareren</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Het declareren van je uren gaat op dit moment nog handmatig. Geef het aantal uren per taak aan, hierna behandelen we je verzoek.</p>
						<br>
						<?php echo do_shortcode( '[contact-form-7 id="8071" title="Uren declareren"]' ); ?>

						<!-- <p>Elke taak moet je apart declareren. Ga naar mijn taken en vul je declaratie in. Op je dashboard kun je zien wanneer je declaratie is goedgekeurd. </p>

						<br>
						<p>Hulp nodig of vragen? Stuur ons dan een bericht of start een discussie</p> -->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal --> 
		<div class="modal fade" id="modal-stemminginfo" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Stem over dit project</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Voor dit project is geen stemming beschikbaar.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal --> 
		<div class="modal fade" id="modal-bronnen" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Bronnen</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>De volgende bronnen zijn nodig voor de uitvoering van deze taak:</p>
						<div class="box paste paste-block" id="paste-block-bronnen"></div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal --> 
		<div class="modal fade" id="modal-resultaten" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Resultaten</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>De volgende resultaten zijn opgeleverd bij deze taak:</p>

						<div class="box paste" id="paste-block-resultaten"></div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
					</div>
				</div>
			</div>
		</div>

		<?php get_footer(); ?>

		<script>
			$('#edit').hide();

			$('#edit-link').click(function(event) {
			event.preventDefault(); //fix reload to homepage
			$('#view').toggle();
			$('#edit').toggle();
		});
	</script>

	<script>$('ul.tabs').each(function(){
  // For each set of tabs, we want to keep track of
  // which tab is active and its associated content
  var $active, $content, $links = $(this).find('a');

  // If the location.hash matches one of the links, use that as the active tab.
  // If no match is found, use the first link as the initial active tab.
  $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
  $active.addClass('active');

  $content = $($active[0].hash);

  // Hide the remaining content
  $links.not($active).each(function () {
  	$(this.hash).hide();
  });

  // Bind the click event handler
  $(this).on('click', 'a', function(e){
    // Make the old tab inactive.
    $active.removeClass('active');
    $content.hide();

    // Update the variables with the new link and content
    $active = $(this);
    $content = $(this.hash);

    // Make the tab active.
    $active.addClass('active');
    $content.show();

    // Prevent the anchor's default click action
    e.preventDefault();
});
});</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

<script>
	$(document).ready( function () {
		
		$('.datatable').DataTable( 
		{
			"language": {
				"sProcessing": "Bezig...",
				"sLengthMenu": "_MENU_ resultaten weergeven",
				"sZeroRecords": "Geen resultaten gevonden",
				"sInfo": "_START_ tot _END_ van _TOTAL_ resultaten",
				"sInfoEmpty": "Geen resultaten om weer te geven",
				"sInfoFiltered": " (gefilterd uit _MAX_ resultaten)",
				"sInfoPostFix": "",
				"sSearch": "Zoeken:",
				"sEmptyTable": "Geen resultaten aanwezig in de tabel",
				"sInfoThousands": ".",
				"sLoadingRecords": "Een moment geduld aub - bezig met laden...",
				"oPaginate": {
					"sFirst": "Eerste",
					"sLast": "Laatste",
					"sNext": "Volgende",
					"sPrevious": "Vorige"
				},
				"oAria": {
					"sSortAscending":  ": activeer om kolom oplopend te sorteren",
					"sSortDescending": ": activeer om kolom aflopend te sorteren"
				}
			}
		}
		);
	});
</script>

<script>

	var lis = Array.from(document.querySelectorAll(".copy-block-item"));
	for (var i = 0; i < lis.length; i++) {
		lis[i].addEventListener("click", function(e) { 
    document.getElementById("paste-block-bronnen").innerHTML = e.target.id; //this replaces the content.
     document.getElementById("paste-block-resultaten").innerHTML = e.target.id; //this replaces the content.

     document.getElementById("paste-block-status").innerHTML = "<? $rows = get_field('taken'); ?>" + e.target.id + "<? echo $rows['2']['status']; ?>";
 });
	}
</script>



